FROM centos:centos7
MAINTAINER Bluesoft <devops@bluesoft.com.br>

# Install the appropriate software
RUN yum -y update;
RUN yum -y install epel-release; yum clean all;
RUN curl --silent --location https://rpm.nodesource.com/setup_4.x | bash - ;
RUN yum -y install bzip2 nodejs freetype fontconfig java-1.8.0-openjdk.x86_64 ;

RUN npm install -g cordova ionic bower grunt-cli phantomjs;