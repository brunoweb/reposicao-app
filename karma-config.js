// Karma configuration
// Generated on Tue Nov 24 2015 10:12:45 GMT-0200 (BRST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'www/lib/ionic/js/ionic.bundle.js',
			'www/lib/angular/angular.js',
			'www/lib/angular-mocks/angular-mocks.js',
			'www/lib/ionic/js/ionic.bundle.js',
			'www/lib/ngCordova/dist/ng-cordova.js',
			'www/lib/underscore/underscore-min.js',
			'www/lib/angular-underscore-module/angular-underscore-module.js',
			'www/lib/angular-ios9-uiwebview-patch/angular-ios9-uiwebview-patch.js',
			'www/app/**/*.js',
			'templates.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },
		
		plugins : [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-coverage',
      "karma-chrome-launcher",
      "karma-htmlfile-reporter",
      'karma-nyan-reporter'
    ],


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: [
			//'progress'
			'coverage',
      'nyan',
      'html'	
		],
		
		coverageReporter: {
      dir : 'coverage/',
      instrumenterOptions: {
        istanbul: { noCompact: true }
      },
      reporters: [
        { type: 'html', subdir: 'html' },
        { type: 'lcovonly', subdir: '.', file: 'report-lcov.lcov'}
      ]

    },
		
		// reporter options
    nyanReporter: {
      // suppress the error report at the end of the test run
      suppressErrorReport: false,

      // increase the number of rainbow lines displayed
      // enforced min = 4, enforced max = terminal height - 1
      numberOfRainbowLines : 4 // default is 4
    },

    htmlReporter: {
      outputFile: '/htmlReport.html',
      
      // Optional 
      pageTitle: 'Unit Tests',
      subPageTitle: 'Consulta de produtos'
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,
  });
};
