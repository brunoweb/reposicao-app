# Consulta de Produtos App #

Consulta de Produtos é um aplicativo desenvolvido com **Ionic** para facilitar a pesquisa de produtos através do gtin (Utilizando a câmera do celular para ler o código de barras) ou através da descrição.
Informações como estoque, custo, preço, venda online e etc são exibidas após a consulta.

### Tecnologias utilizadas ###

* [Ionic](http://ionicframework.com/)
* [Angular.js](https://angularjs.org/)
* [Cordova](https://cordova.apache.org/)
* [Karma](http://karma-runner.github.io/0.13/index.html)
* [Jasmine](http://jasmine.github.io/)

### Set up ###

#### 1) Clona o repositório:

```
#!dos
git clone git@bitbucket.org:bluesoftbr/consulta-produtos-app.git

```

#### 2) Execute o npm para instalar todas as dependências:

```
#!dos
npm install

```


No arquivo package.json há uma configuração para baixar todas as dependências do projeto (Bower, Cordova Plugins e etc), se **não** for instalado corretamente, realize manualmente a instalação de cada um deles:

##### Bower:
```
#!dos
bower install

```

##### Cordova Plugins:
```
#!dos
ionic platform remove android
ionic platform remove ios

ionic platform add android
ionic platform add ios

ionic state reset

```


#### 3) Configure o sub-módulo commons:

Se sua pasta www/app/commons estiver **vazia**, utilize os passos no link a seguir para adicionar o sub-módulo do git:

[bluesoft-app-commons](https://bitbucket.org/bluesoftbr/bluesoft-app-commons)


#### 4) Url API Endpoint:

Após todas as dependências instaladas com sucesso configure o API Endpoint para sua máquina local ou remota:

*Obs: O Ionic para ser rodado localmente através do browser necessita de um proxy configurado. Esse proxy encontra-se no arquivo* **ionic.project** *e a URL do API Endpoint precisa apontar para o endereço do servidor local do Ionic e o pattern do proxy (erp-web) como mostrado na linha com asteriscos.*

**www/app/commons/constant/api-endpoint.js**

```
#!javascript

(function() {
	'use strict';
	// jscs: disable
	angular.module('bluesoft-app')
			.constant('ApiEndpoint',
             {
			 url: 'http://localhost:8100/erp-web' // DEV-ERP-WEB (LOCALHOST)   /// ********
				// url: 'http://localhost:8100/beta' // DEV-BETA
//				url: 'http://192.168.253.138:8080/erp-web' // MOBILE (LOCALHOST)
//				url: 'https://erp-2a.bluesoft.com.br/beta' // MOBILE (BETA)
//  url: 'https://erp-2a.bluesoft.com.br/{0}' // MOBILE (PRODUCAO)
    });
	// jscs: enable

})();

```

#### 5) Executar via browser:


```
#!dos

ionic serve --lab
```


*Lembre-se de sempre deixar rodando o grunt para analisar o javascript (jshint, jscs).


```
#!dos

grunt dev
```



### Testes ###

Os testes são desenvolvidos utilizando Karma e Jasmine.

Para executá-los utilize:


```
#!dos

grunt test
```

### Publicar apps ###


### Google Play ###

Não esqueça de adicionar as seguintes configurações dentro da seção *android { }* do arquivo *platforms/android/build.gradle*:

```
#!javascript
android {
    ...

    lintOptions {
        checkReleaseBuilds false
        abortOnError false
    }

    ...
}
```

Execute o shell script *build-android.sh* na raiz do projeto:

*Obs: informe a senha **bluesoft8437** *:


```
#!shellscript

sh build-android.sh
```

###  ###
###  ###
###  ###

*Let's coding!!!*