angular.module('bluesoft-app').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/commons/directive/bs-parceiros/bs-parceiros.html',
    " <div class=\"panel-parceiros padding-top\">\n" +
    " \t<h5>Parceiros:</h5>\n" +
    " \t<div class=\"row parceiros\">\n" +
    " \t\t<div class=\"col\">\n" +
    " \t\t\t<a href=\"#\" onclick=\"window.open('https://aws.amazon.com', '_system', 'location=yes'); return false;\">\n" +
    " \t\t\t\t<img src=\"app/commons/img/login/logo_aws.png\" class=\"img-responsive\" alt=\"AWS\" />\n" +
    " \t\t\t</a>\n" +
    " \t\t</div>\n" +
    " \t\t<div class=\"col\">\n" +
    " \t\t\t<a href=\"#\" onclick=\"window.open('http://www.tableau.com', '_system', 'location=yes'); return false;\">\n" +
    " \t\t\t\t<img src=\"app/commons/img/login/logo_tableau.png\" class=\"img-responsive\" alt=\"Tableau\" />\n" +
    " \t\t\t</a>\n" +
    " \t\t</div>\n" +
    " \t</div>\n" +
    " </div>\n"
  );


  $templateCache.put('app/commons/login/template/login-tenant-template.html',
    "<ion-view hide-nav-bar=\"true\" >\n" +
    "\t<ion-content overflow-scroll=\"false\" class=\"padding login-container\">\n" +
    "\t\t<div class=\"logo-container\">\n" +
    "\t\t\t<img class=\"img-responsive\" src=\"app/commons/img/bslogo.png\" alt=\"Bluesoft\" />\n" +
    "\t\t</div>\n" +
    "\n" +
    "\t\t<form name=\"vm.form\">\n" +
    "\t\t\t<div class=\"list list-inset\">\n" +
    "\t\t\t\t<label class=\"item item-input\">\n" +
    "\t\t\t\t\t<input type=\"text\" class=\"empresa\" placeholder=\"Empresa\" ng-model=\"vm.empresa\" autocapitalize=\"none\" autocorrect=\"off\" autofocus=\"true\" required />\n" +
    "\t\t\t\t</label>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<button class=\"button button-block button-bluesoft\" ng-click=\"vm.selecionarTenant()\" ng-disabled=\"vm.form.$invalid\">Entrar</button>\n" +
    "\t\t</form>\n" +
    "\n" +
    "\t\t<bs-parceiros />\n" +
    "\t</ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('app/commons/login/template/login-usuario-template.html',
    "<ion-view view-title=\"\" >\n" +
    "\t<ion-nav-bar class=\"bar-bluesoft-gray\">\n" +
    "\t\t<ion-nav-buttons side=\"left\">\n" +
    "\t\t\t<button class=\"button button-icon ion-ios-arrow-left\" ng-click=\"vm.back()\"></button>\n" +
    "\t\t</ion-nav-buttons>\n" +
    "\t</ion-nav-bar>\n" +
    "\n" +
    "\t<ion-content overflow-scroll=\"false\" class=\"padding login-container\">\n" +
    "\t\t<i class=\"logo-tenant\" alt=\"{{ ::vm.empresa.tenant }}\" ng-style=\"{ 'background-image': 'url({{ ::vm.empresa.urlLogo }})' }\"></i>\n" +
    "\n" +
    "\t\t<form name=\"vm.form\" >\n" +
    "\t\t\t<div class=\"list list-inset\">\n" +
    "\t\t\t\t<label class=\"item item-input\">\n" +
    "\t\t\t\t\t<input type=\"text\" placeholder=\"Usuário\" ng-model=\"vm.usuario\" autocapitalize=\"none\" autocorrect=\"off\" autofocus=\"true\" required />\n" +
    "\t\t\t\t</label>\n" +
    "\t\t\t\t<label class=\"item item-input\">\n" +
    "\t\t\t\t\t<input type=\"password\" placeholder=\"Senha\" ng-model=\"vm.senha\" autocapitalize=\"none\" autocorrect=\"off\" required />\n" +
    "\t\t\t\t</label>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<button class=\"button button-block button-bluesoft\" ng-click=\"vm.entrar()\" ng-disabled=\"vm.form.$invalid\">Entrar</button>\n" +
    "\t\t</form>\n" +
    "\n" +
    "\t\t<bs-parceiros />\n" +
    "\t</ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('app/consulta-produto/template/consulta-produto-template.html',
    "<ion-view hide-nav-bar=\"false\" id=\"home\" title=\"\">\n" +
    "\t<ion-nav-buttons side=\"right\">\n" +
    "\t\t<button class=\"button button-icon ion-ios-person\" ng-click=\"$root.popover.show($event)\"></button>\n" +
    "\t</ion-nav-buttons>\n" +
    "\n" +
    "\t<ion-content class=\"padding\">\n" +
    "\t\t<div class=\"logo\">\n" +
    "\t\t\t<img class=\"logo-tenant\" src=\"app/commons/img/bslogo.png\" />\n" +
    "\t\t</div>\n" +
    "\n" +
    "\t\t<form class=\"form-container\">\n" +
    "\t\t\t<div class=\"item item-input item-floating-label\">\n" +
    "\t\t\t\t<span class=\"input-label\">GTIN ou Nome do Produto</span>\n" +
    "\t\t\t\t<input ng-model=\"vm.query\" type=\"search\" placeholder=\"GTIN ou Nome do Produto\" ng-focus=\"\">\n" +
    "\t\t\t\t<span class=\"highlight\"></span>\n" +
    "\t\t\t\t<button class=\"button button-icon ion-ios-camera\" ng-click=\"vm.buscarClick()\" ></button>\n" +
    "\t\t\t</div>\n" +
    "\t\t</form>\n" +
    "\t</ion-content>\n" +
    "</ion-view>"
  );


  $templateCache.put('app/consulta-produto/template/usuario-popover-template.html',
    "<ion-popover-view>\n" +
    "\t<ion-content>\n" +
    "\t\t<div class=\"list\">\n" +
    "\t\t\t<div class=\"item item-divider item-text-wrap\">\n" +
    "\t\t\t\tOlá, <strong><span ng-bind=\"::usuario\"></span></strong>\n" +
    "\t\t\t</div>\n" +
    "\t\t\t<a ui-sref=\"menu.historico-consultas()\" ng-click=\"$root.popover.hide()\" class=\"item\" ng-if=\"$root.possuiHistoricoConsultas\">Ver histórico</a>\n" +
    "\t\t\t<a ng-click=\"sair()\" class=\"item\">Sair</a>\n" +
    "\t\t</div>\n" +
    "\t</ion-content>\n" +
    "</ion-popover-view>\n"
  );


  $templateCache.put('app/core/directive/bs-header-menu/bs-header-menu-template.html',
    "<div class=\"actions-container\">\n" +
    "\t<button class=\"button button-outline button-stable\" ng-click=\"historico()\" ng-if=\"$root.menu.historico\">Ver histórico</button>\n" +
    "\t<button class=\"button button-outline button-stable\" ng-click=\"escanearGTIN()\" ng-if=\"$root.menu.escanear\">Escanear GTIN</button>\n" +
    "</div>"
  );


  $templateCache.put('app/historico-consultas/template/historico-consultas-template.html',
    "<ion-view title=\"Consultas\" id=\"pagina-consulta\">\n" +
    "\t<ion-content padding=\"true\" class=\"has-footer\">\n" +
    "\t\t<div class=\"timeline-container\">\n" +
    "\t\t\t<a class=\"item item-icon-right item-text-wrap timeline-item\" ng-click=\"vm.buscar(historicoConsulta.query)\" ng-repeat=\"historicoConsulta in vm.historicoConsultas\">\n" +
    "\t\t\t\t<h2 ng-bind=\"::historicoConsulta.query\"></h2>\n" +
    "\t\t\t\t<p ng-bind=\"::historicoConsulta.produtoDescricao\"></p>\n" +
    "\n" +
    "\t\t\t\t<div class=\"time\">\n" +
    "\t\t\t\t\t<span ng-bind=\"::vm.convertToDate(historicoConsulta.data) | date:'dd/MM/yyyy'\"></span>\n" +
    "\t\t\t\t\t<small class=\"stable\" ng-bind=\"::vm.convertToDate(historicoConsulta.data) | date:'HH:mm'\"></small>\n" +
    "\t\t\t\t</div>  \n" +
    "\t\t\t\t\n" +
    "\t\t\t\t<i class=\"icon ion-ios-arrow-right\"></i>\n" +
    "\t\t\t</a>\n" +
    "\t\t</div>\n" +
    "\t</ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('app/info-produto-loja-detalhe/template/info-produto-loja-detalhe-template.html',
    "<ion-view title=\"Loja {{ vm.infoEstoque.lojaKey }}\">\n" +
    "\t\t<ion-nav-buttons side=\"left\">\n" +
    "\t\t\t<a class=\"button button-icon button-icon-white ion-ios-arrow-left\" ng-click=\"vm.voltar()\"></a>\n" +
    "\t\t</ion-nav-buttons>\n" +
    "\t<ion-content class=\"has-footer\">\n" +
    "\t\t<ion-list class=\"produto-detalhes\">\n" +
    "\t\t\t<ion-item class=\"item-avatar item-text-wrap\">\n" +
    "\t\t\t\t<img ng-if=\"!vm.urlFoto\" src=\"./img/placeholder.png\">\n" +
    "\t\t\t\t<img ng-if=\"vm.urlFoto\" src=\"{{ vm.urlFoto }}\">\n" +
    "\t\t\t\t<h2 ng-bind=\"::vm.produto.descricao\"></h2>\n" +
    "\t\t\t\t<p>GTIN: <span ng-bind=\"::vm.produto.gtin\"></span></p>\n" +
    "\t\t\t</ion-item>\n" +
    "\t\t</ion-list>\n" +
    "\t\t\n" +
    "\t\t<div ng-if=\"vm.infoEstoque.onlineQuantidade && vm.infoEstoque.onlineValor\">\n" +
    "\t\t\t<h4 class=\"dl-divider\">Venda Online <i class=\"fa fa-icon-right fa-bar-chart\"></i></h4>\n" +
    "\t\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Quantidade:</dt>  \n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.onlineQuantidade  | number: 2\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Preço Médio:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.onlinePrecoMedio | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t\t\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Valor:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.onlineValor | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t\t\n" +
    "\t\t\t</dl>\n" +
    "\t\t</div>\n" +
    "\t\t\n" +
    "\t\t<h4 class=\"dl-divider\">Estoques <i class=\"fa fa-icon-right fa-database\"></i></h4>\n" +
    "\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Estoque Disponível:</dt>  \n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.estoqueQueEstaDisponivel\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\t\t\t\n" +
    "\t\t\t<div ng-if=\"vm.infoEstoque.estoqueReservado  && vm.infoEstoque.estoqueReservado > 0\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Estoque Reservado:</dt>  \n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.estoqueReservado \"></dd>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div ng-if=\"vm.infoEstoque.estoqueEmTerceiros && vm.infoEstoque.estoqueEmTerceiros > 0\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Estoque em Terceiros:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.estoqueEmTerceiros\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div ng-if=\"vm.infoEstoque.estoqueFuturo  && vm.infoEstoque.estoqueFuturo > 0\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Estoque Futuro:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.estoqueFuturo \"></dd>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Estoque Total:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.estoqueTotal(vm.infoEstoque)\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Cobertura de estoque:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.estoqueCobertura \"></dd>\n" +
    "\t\t\t</div>\n" +
    "\t\t\t\n" +
    "\t\t\t<div ng-if=\"vm.infoEstoque.estoquePendenteEmPedido > 0\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Quantidade em Pedidos Pendentes:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.estoquePendenteEmPedido\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Quantidade Exposição:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.quantidadeExposicao\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\t\t\t\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Quantidade Ponto Extra:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.quantidadePontoExtra\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\t\t\t\n" +
    "\t\t</dl>\n" +
    "\t\t\n" +
    "\t\t<h4 class=\"dl-divider\">Custos de Estoque <i class=\"fa fa-icon-right fa-money\"></i></h4>\n" +
    "\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Custo Bruto:</dt>  \n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.custoSemIcm | currency:'R$ '\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Custo Líquido:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.custoComIcm | currency:'R$ '\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t<dt>Custo Contábil:</dt>\n" +
    "\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.custoContabil | currency:'R$ '\"></dd>\n" +
    "\t\t\t</div>\n" +
    "\t\t</dl>\n" +
    "\t\t\n" +
    "\t\t<div ng-if=\"vm.infoEstoque.preco\">\n" +
    "\t\t\t<h4 class=\"dl-divider\">Preços em vigor <i class=\"fa fa-icon-right fa-usd\"></i></h4>\n" +
    "\t\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Preço Atual:</dt>  \n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.preco | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Preço Fidelidade:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.precoFidelidade | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Preço Oferta <span ng-if=\"vm.infoEstoque.precoOfertaPeriodo\" ng-bind=\"::vm.infoEstoque.precoOfertaPeriodo\"></span>: </dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.precoOferta | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t</dl>\n" +
    "\t\t</div>\t\n" +
    "\t\t\n" +
    "\t\t<div ng-if=\"vm.infoEstoque.margemBrutaEstoque && vm.infoEstoque.margemLiquidaEstoque\">\n" +
    "\t\t\t<h4 class=\"dl-divider\">Margens <i class=\"fa fa-icon-right fa-percent\"></i></h4>\n" +
    "\t\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Margem Bruta:</dt>  \n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.margemBrutaEstoque | number: 2\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Margem Líquida:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.margemLiquidaEstoque | number: 2\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t</dl>\n" +
    "\t\t</div>\n" +
    "\t\t\n" +
    "\t\t<div ng-if=\"vm.infoEstoque.estoqueTroca\">\n" +
    "\t\t\t<h4 class=\"dl-divider\">Estoque de trocas <i class=\"fa fa-icon-right fa-tag\"></i> </h4>\n" +
    "\t\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Quantidade:</dt>  \n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.estoqueTroca \"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Custo Bruto:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.custoUnitarioComIcm | currency:'R$ ' \"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Custo Líquido:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.custoUnitario | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Custo Contábil:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.custoUnitarioContabil | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t</dl>\n" +
    "\t\t</div>\n" +
    "\t\t\n" +
    "\t\t<div ng-if=\"vm.infoEstoque.compraFornecedor\">\n" +
    "\t\t\t<h4 class=\"dl-divider\">Últimas Compras <i class=\"fa fa-icon-right fa-shopping-cart\"></i></h4>\n" +
    "\t\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Fornecedor:</dt>  \n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.compraFornecedor\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Data:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.compraData \"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Unidades:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.compraQuantidadeUnitario\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t<dt>Custo:</dt>\n" +
    "\t\t\t\t\t<dd ng-bind=\"::vm.infoEstoque.compraCusto | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t</dl>\n" +
    "\t\t</div>\n" +
    "\t\t\n" +
    "\t</ion-content>\n" +
    "</ion-view>\n" +
    "\n"
  );


  $templateCache.put('app/info-produto-loja/template/info-produto-loja-template.html',
    "<ion-view title=\"Estoque do Produto por Loja\">\n" +
    "\t<ion-nav-buttons side=\"left\">\n" +
    "\t\t\t<a class=\"button button-icon button-icon-white ion-ios-arrow-left\" ng-click=\"vm.voltar()\"></a>\n" +
    "\t</ion-nav-buttons>\n" +
    "\t\t<ion-content class=\"has-footer\">\n" +
    "\t\t\t<ion-list class=\"produto-detalhes\">\n" +
    "\t\t\t\t<ion-item class=\"item-avatar item-text-wrap\">\n" +
    "\t\t\t\t\t<img ng-if=\"!vm.info.urlFoto\" src=\"./img/placeholder.png\">\n" +
    "\t\t\t\t\t<img ng-if=\"vm.info.urlFoto\" src=\"{{ vm.info.urlFoto }}\">\n" +
    "\t\t\t\t\t<h2 ng-bind=\"::vm.info.produto.descricao\"></h2>\n" +
    "\t\t\t\t\t<p>GTIN: <span ng-bind=\"::vm.info.produto.gtin\"></span></p>\n" +
    "\t\t\t\t</ion-item>\n" +
    "\t\t\t</ion-list>\n" +
    "\n" +
    "\t\t\t<div class=\"list card\" ng-repeat=\"infoEstoque in vm.info.informacoesEstoque\">\n" +
    "\t\t\t\t<a ng-click=\"vm.detalharLoja(vm.info.produto, infoEstoque, vm.info.urlFoto)\" class=\"item item-icon-right link\" >\n" +
    "\t\t\t\t\t\t<i class=\"icon ion-ios-arrow-right\"></i>\n" +
    "\t\t\t\t\t\t<h2 class=\"item item-divider\">Loja <span ng-bind=\"::infoEstoque.lojaKey\"></span></h2>\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\t<dl class=\"dl-horizontal\">\n" +
    "\t\t\t\t\t\t\t<div ng-if=\"infoEstoque.estoqueQueEstaDisponivel\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t\t\t\t<dt>Estoque Disponível:</dt>  \n" +
    "\t\t\t\t\t\t\t\t<dd ng-bind=\"infoEstoque.estoqueQueEstaDisponivel\"></dd>\n" +
    "\t\t\t\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t\t\t\t<div ng-if=\"infoEstoque.preco\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t\t\t\t<dt>Preço de Venda:</dt>\n" +
    "\t\t\t\t\t\t\t\t<dd ng-bind=\"infoEstoque.preco | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t\t\t\t<div ng-if=\"infoEstoque.precoOferta\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t\t\t\t<dt>Preço de Oferta:</dt>\n" +
    "\t\t\t\t\t\t\t\t<dd ng-bind=\"infoEstoque.precoOferta | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t\t\t\t<div ng-if=\"infoEstoque.onlineQuantidade\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t\t\t\t<dt>Quantidade Online:</dt>\n" +
    "\t\t\t\t\t\t\t\t<dd ng-bind=\"infoEstoque.onlineQuantidade\"></dd>\n" +
    "\t\t\t\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t\t\t\t<div ng-if=\"infoEstoque.onlinePrecoMedio\" class=\"item-text-wrap\">\n" +
    "\t\t\t\t\t\t\t\t<dt>Preço Médio Online:</dt>\n" +
    "\t\t\t\t\t\t\t\t<dd ng-bind=\"infoEstoque.onlinePrecoMedio | currency:'R$ '\"></dd>\n" +
    "\t\t\t\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t\t\t</dl>\n" +
    "\t\t\t\t</a>\n" +
    "\t\t\t</div>\n" +
    "\t\t</ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('app/menu/template/menu-template.html',
    "<ion-view>\n" +
    " <ion-nav-bar class=\"bar-stable\" ng-class=\"{ 'has-menu': $root.menu }\">\n" +
    "\t\t<ion-nav-buttons side=\"left\">\n" +
    "\t\t\t<a ui-sref=\"menu.home()\" class=\"button button-icon button-icon-white ion-android-home\" ng-class=\"{ 'hide': !$root.menu }\"></a>\n" +
    "\t\t</ion-nav-buttons>\n" +
    "\t\t<bs-header-menu></bs-header-menu>\n" +
    "\t\t<ion-nav-buttons side=\"right\">\n" +
    "\t\t\t<a ng-click=\"$root.buscar = !$root.buscar\" class=\"button button-icon button-icon-white {{ $root.buscar ? 'ion-android-close' : 'ion-ios-search-strong' }}\"></a>\n" +
    "\t\t</ion-nav-buttons>\n" +
    " </ion-nav-bar>\n" +
    "<div class=\"bar bar-dark bar-subheader item-input-inset\" ng-show=\"$root.buscar\" ng-controller=\"MenuController as vm\">\n" +
    "  <div class=\"item-input-wrapper\">\n" +
    "    <input type=\"search\" placeholder=\"GTIN ou Nome do produto\" ng-model=\"$root.query\" />\n" +
    "  </div>\n" +
    "  <button class=\"button button-clear ion-ios-search-strong\" ng-click=\"vm.buscar($root.query)\"></button>\n" +
    "</div>\n" +
    " <ion-nav-view ng-class=\"{ 'has-header': $root.buscar }\" name=\"menuContent\"></ion-nav-view>\n" +
    "</ion-view>"
  );


  $templateCache.put('app/produtos/template/produtos-template.html',
    "<ion-view title=\"Busca por '{{ vm.descricao }}'\">\n" +
    "\t<ion-content padding=\"true\" class=\"has-footer\">\n" +
    "\t\t<div class=\"list card\" ng-repeat=\"produto in vm.produtos\">\n" +
    "\t\t\t<a class=\"item item-thumbnail-left item-text-wrap item-icon-right link\" ng-click=\"vm.detalharProduto(produto)\">\n" +
    "\t\t\t\t<img ng-if=\"!produto.urlFoto\" src=\"./img/placeholder.png\">\n" +
    "\t\t\t\t<img ng-if=\"produto.urlFoto\" src=\"{{ produto.urlFoto }}\">\n" +
    "\t\t\t\t<h2 ng-bind=\"::produto.descricao\"></h2>\n" +
    "\t\t\t\t<p>GTIN: <span ng-bind=\"::produto.gtin\"></span></p>\n" +
    "\t\t\t\t<p ng-if=\"produto.estoqueTotal\">Estoque Disponível: {{ produto.estoqueTotal }} </p>\n" +
    "\t\t\t\t<p ng-if=\"produto.precoAtual\">Preço de Venda: {{ produto.precoAtual | currency:'R$ ' }}</p>\n" +
    "\t\t\t\t<i class=\"icon ion-ios-arrow-right\"></i>\n" +
    "\t\t\t</a>\n" +
    "\t\t</div>\n" +
    "\t</ion-content>\n" +
    "</ion-view>"
  );

}]);
