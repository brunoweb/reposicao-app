(function() {
    'use strict';

    var URL_TENANT = 'URL_TENANT';

    describe('ConsultaProdutoApiService', function() {
        var ConsultaProdutoApiService,
        $localstorage,
        $httpBackend,
        url = {
            listarInformacoesPorLoja: '/erp-app/areas/compras/consulta-produto-app/listarInformacoesPorLoja.action',
            listarProdutosPorDescricao: '/erp-app/areas/compras/consulta-produto-app/listarPorDescricao.action'
        };

        beforeEach(function() {
            module('bluesoft-app');
        });

        beforeEach(inject(function(_ConsultaProdutoApiService_, _$localstorage_, _$httpBackend_) {
            $localstorage = _$localstorage_;
            $httpBackend = _$httpBackend_;
            ConsultaProdutoApiService = _ConsultaProdutoApiService_;

            spyOn($localstorage, 'getObject').and.returnValue(URL_TENANT);
        }));

        it('deve listar informacoes por loja', deveListarInformacoesPorLoja);
        it('deve listar produtos por descricao', deveListarProdutosPorDescricao);

        function deveListarInformacoesPorLoja() {
            var produto = {
                informacoesEstoque: [{}, {}]
            }

            $httpBackend.expectGET(URL_TENANT + url.listarInformacoesPorLoja + '?gtin=true&produto=21').respond(produto);
            ConsultaProdutoApiService.listarInformacoesPorLoja(21);
            $httpBackend.flush();
        }

        function deveListarProdutosPorDescricao() {
            $httpBackend.expectGET(URL_TENANT + url.listarProdutosPorDescricao + '?descricao=manga').respond('ok');
            ConsultaProdutoApiService.listarProdutosPorDescricao('manga');
            $httpBackend.flush();
        }
    });
})();
