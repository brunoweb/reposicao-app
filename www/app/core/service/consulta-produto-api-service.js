(function() {
    'use strict';

    angular.module('bluesoft-app').factory('ConsultaProdutoApiService', ConsultaProdutoApiService);

    ConsultaProdutoApiService.$inject = ['UserInfo', 'bsRequest'];

    function ConsultaProdutoApiService(UserInfo, bsRequest) {
        var baseUrl = '/api/consulta-produto-app/produtos',
            service = {
                listarInformacoesPorCodigoBarra: listarInformacoesPorCodigoBarra,
                listarProdutosPorDescricao: listarProdutosPorDescricao,
                obterFotos: obterFotos
            };

        return service;

        function listarInformacoesPorCodigoBarra(params) {
            return bsRequest.get({
                url: getUrl('/codigo-barra/' + params.codigoBarra)
            });
        }

        function listarProdutosPorDescricao(params) {
            return bsRequest.get({
                url: getUrl(''),
                params: params
            });
        }

        function obterFotos(params) {
            return bsRequest.get({
                url: getUrl('/fotos'),
                params: params,
                hideErrorMessage: true,
                hideLoader: true
            });
        }

        function getUrl(url) {
            return UserInfo.usuario.urlTenant + baseUrl + url;
        }
    }
})();
