(function() {
    'use strict';

    describe('ConsultaProdutoService', function() {
        var ConsultaProdutoService,
            ConsultaProdutoApiService,
            HistoricoConsultasLocalService,
            $cordovaBarcodeScanner,
            mockPromises,
            $q,
            $scope,
            $state,
            $dialogs;

        beforeEach(function() {
            module('bluesoft-app');
        });

        beforeEach(inject(function(_$q_, _ConsultaProdutoService_, _ConsultaProdutoApiService_,_HistoricoConsultasLocalService_,_$cordovaBarcodeScanner_, $rootScope, _$state_, _$dialogs_) {
            $q = _$q_;
            ConsultaProdutoService = _ConsultaProdutoService_;
            ConsultaProdutoApiService = _ConsultaProdutoApiService_;
            HistoricoConsultasLocalService = _HistoricoConsultasLocalService_;
            $cordovaBarcodeScanner = _$cordovaBarcodeScanner_;
            $scope = $rootScope.$new();
            $state = _$state_;
            $dialogs = _$dialogs_;

            mockPromises = {
                ConsultaProdutoApiService: {
                    obj: ConsultaProdutoApiService,
                    promises: {
                        listarProdutosPorDescricao: $q.defer(),
                        listarInformacoesPorLoja: $q.defer()
                    }
                },

                HistoricoConsultasLocalService: {
                    obj: HistoricoConsultasLocalService,
                    promises: {
                        salvar: $q.defer()
                    }
                },

                $cordovaBarcodeScanner: {
                    obj: $cordovaBarcodeScanner,
                    promises: {
                        scan: $q.defer()
                    }
                }
            }

            spyOn($state, 'go');
            spyOn($dialogs, 'showAlert');

            var keyMock,
            mock,
            key;

            for (keyMock in mockPromises) {
                mock = mockPromises[keyMock];
                for (key in mock.promises) {
                    spyOn(mock.obj, key).and.returnValue(mock.promises[key].promise);
                }
            }
        }));

        it('deve buscar por descricao', deveBuscarPorDescricao);
        it('deve buscar por gtin produto com uma loja', deveBuscarPorGtinUmaLoja);
        it('deve buscar por gtin produto com varias lojas', deveBuscarPorGtinVariasLojas);
        it('deve buscar mas nao salvar historico', deveBuscarMasNaoSalvarHistorico);
        it('deve buscar e salvar historico', deveBuscarESalvarHistorico);
        it('deve escanear codigo de barras', deveEscanearCodigoDeBarras);
        it('deve limpar variaveis de controle', deveLimparVariaveisDeControle);
        it('deve retornar alerta de que o produto não foi encontrado pelo GTIN', deveRetornarAlertaDeQueOProdutoNaoFoiEncontradoPeloGtin);
        it('deve retornar alerta de que o produto não foi encontrado peloa Descricao', deveRetornarAlertaDeQueOProdutoNaoFoiEncontradoPelaDescricao);

        function deveBuscarPorDescricao() {
            var data =  { data: {informacoesEstoque: [{}, {}]}},
            descricao = 'manga';

            mockPromises.ConsultaProdutoApiService.promises.listarProdutosPorDescricao.resolve(data);
            ConsultaProdutoService.buscar(descricao);
            $scope.$apply();

            expect(ConsultaProdutoApiService.listarProdutosPorDescricao).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith('menu.produtos', {descricao: descricao, produtos: data.data});
        }

        function deveBuscarPorGtinUmaLoja() {
            var data = {
                data: {
                    informacoesEstoque: [{}, {}],
                    produto: {}
                }
            };

            mockPromises.ConsultaProdutoApiService.promises.listarInformacoesPorLoja.resolve(data);
            ConsultaProdutoService.buscar(123);
            $scope.$apply();

            expect(ConsultaProdutoApiService.listarInformacoesPorLoja).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith('menu.info-produto-loja-detalhe', { infoEstoque: data.data.informacoesEstoque[0], produto: data.data.produto });
        }

        function deveBuscarPorGtinVariasLojas() {
            var data = { data: {informacoesEstoque: [{}, {}, {}]}};

            mockPromises.ConsultaProdutoApiService.promises.listarInformacoesPorLoja.resolve(data);
            ConsultaProdutoService.buscar(123);
            $scope.$apply();

            expect(ConsultaProdutoApiService.listarInformacoesPorLoja).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith('menu.info-produto-loja', { produto: data.data });
        }

        function deveBuscarMasNaoSalvarHistorico() {
            mockPromises.ConsultaProdutoApiService.promises.listarInformacoesPorLoja.resolve({ data: {informacoesEstoque: [{}, {}]}});
            ConsultaProdutoService.buscar(123, true);
            $scope.$apply();

            expect(HistoricoConsultasLocalService.salvar).not.toHaveBeenCalled();
        }

        function deveBuscarESalvarHistorico() {
            mockPromises.ConsultaProdutoApiService.promises.listarInformacoesPorLoja.resolve({ data: {informacoesEstoque: [{}, {}]}});
            ConsultaProdutoService.buscar(123, false);
            $scope.$apply();

            expect(HistoricoConsultasLocalService.salvar).toHaveBeenCalled();
        }

        function deveEscanearCodigoDeBarras() {
            ConsultaProdutoService.escanearGtin();
            expect($cordovaBarcodeScanner.scan).toHaveBeenCalled();
        }

        function deveLimparVariaveisDeControle() {
            mockPromises.ConsultaProdutoApiService.promises.listarInformacoesPorLoja.resolve({ data: {informacoesEstoque: [{}, {}]}});
            ConsultaProdutoService.buscar(123);
            $scope.$apply();

            expect($scope.query).toBe('');
            expect($scope.buscar).toBe(false);
        }

        function deveRetornarAlertaDeQueOProdutoNaoFoiEncontradoPeloGtin() {
            var data = { };

            mockPromises.ConsultaProdutoApiService.promises.listarInformacoesPorLoja.resolve(data);
            ConsultaProdutoService.buscar(123);
            $scope.$apply();

            expect(ConsultaProdutoApiService.listarInformacoesPorLoja).toHaveBeenCalled();
            expect($dialogs.showAlert).toHaveBeenCalledWith('Produto não encontrado');
        }

        function deveRetornarAlertaDeQueOProdutoNaoFoiEncontradoPelaDescricao() {
            var data = { };

            mockPromises.ConsultaProdutoApiService.promises.listarProdutosPorDescricao.resolve(data);
            ConsultaProdutoService.buscar('manga');
            $scope.$apply();

            expect(ConsultaProdutoApiService.listarProdutosPorDescricao).toHaveBeenCalled();
            expect($dialogs.showAlert).toHaveBeenCalledWith('Não encontrado produtos com a descrição informada.');
        }
    });
})();
