(function() {
    'use strict';

    angular.module('bluesoft-app').service('ConsultaProdutoService', ConsultaProdutoService);

    ConsultaProdutoService.$inject = ['$state', '$dialogs', '$ionicLoading', '$cordovaBarcodeScanner', '$rootScope', 'ConsultaProdutoApiService', 'HistoricoConsultasLocalService','bsAnalytics'];

    function ConsultaProdutoService($state, $dialogs, $ionicLoading, $cordovaBarcodeScanner, $rootScope, ConsultaProdutoApiService, HistoricoConsultasLocalService, bsAnalytics) {
        var service = {
            buscar: buscar,
            buscarFotos: buscarFotos,
            escanearCodigoBarra: escanearCodigoBarra
        },
        naoSalvarHistoricoConsulta = false,
        scanningBarCode = false;

        function buscar(query, naoSalvarHistorico) {
            if (query) {
                $ionicLoading.show({
                    template: 'Aguarde . . .'
                });

                naoSalvarHistoricoConsulta = naoSalvarHistorico;
                if (!isNaN(query)) {
                    buscarPorCodigoBarra(query);
                } else {
                    buscarPorDescricao(query);
                }
            }
        }

        function buscarFotos(produtos) {
            var produtoKeys = _.map(produtos, function(produto) {
                return produto.produtoKey;
            });

            ConsultaProdutoApiService.obterFotos({produtoKeys: produtoKeys}).then(function doObterFotos(res) {
                preencherFoto(res.data, produtos);
            });
        }

        function preencherFoto(produtosFoto, produtos) {
            var i = 0,
                produtoFoto,
                produto;

            for (i; i < produtosFoto.length; i++) {
                produtoFoto = produtosFoto[i];

                /*jshint loopfunc: true */
                produto = _.find(produtos, function(produto) {
                    return produto.produtoKey === produtoFoto.produtoKey;
                });

                if (produto && produtoFoto.urlFoto) {
                    produto.urlFoto = produtoFoto.urlFoto;
                }
            }
        }

        function escanearCodigoBarra() {
            if (scanningBarCode) {
                return;
            }

            bsAnalytics.trackEvent('Search', 'BarCodeScanner', 'Open camera');
            scanningBarCode = true;
            $cordovaBarcodeScanner.scan().then(function(res) {
                scanningBarCode = false;
                var codigoBarra = res.text;
                buscar(codigoBarra);
            }, function(reason) {
                bsAnalytics.trackException('BarCodeScanner:' + reason, false);

                scanningBarCode = false;
                $dialogs.showAlert('Ocorreu um erro.', reason);
            });
        }

        function buscarPorCodigoBarra(codigoBarra) {
            var params = {codigoBarra: codigoBarra};

            bsAnalytics.trackEvent('Search', 'SearchWithBarCode', codigoBarra, 1);
            ConsultaProdutoApiService
            .listarInformacoesPorCodigoBarra(params)
            .then(function onSearchComplete(res) {
                var produto = res.data;
                if (produto.informacoesLoja) {

                    if (produto.informacoesLoja.length === 1) {
                        $state.go('menu.info-produto-loja-detalhe', {
                            produto: produto,
                            informacoesLoja: produto.informacoesLoja[0]
                        });
                    } else {
                        $state.go('menu.info-produto-loja', {
                            produto: produto
                        });
                    }
                    finalizarBusca(codigoBarra, produto);
                } else {
                    bsAnalytics.trackException('SearchWithBarCode: Produto não encontrado', false);
                    $dialogs.showAlert('Produto não encontrado');
                }
            })
            .finally($ionicLoading.hide);
        }

        function buscarPorDescricao(descricao) {
            var params = {descricao: descricao};

            bsAnalytics.trackEvent('Search', 'SearchWithDescription', descricao, 1);
            ConsultaProdutoApiService
            .listarProdutosPorDescricao(params).then(
                function onSearchComplete(res) {
                    if (!res.data || res.data.length === 0) {
                        bsAnalytics.trackException('SearchWithDescription: Não encontrado produtos com a descrição informada.', false);
                        $dialogs.showAlert('Não encontrado produtos com a descrição informada.');
                    } else {
                        $state.go('menu.produtos', {descricao: descricao, produtos: res.data});
                        finalizarBusca(descricao);
                    }
                },

                function(data, status, headers, config, statusText) {
                    bsAnalytics.trackException('SearchWithDescription: status [' + status + ' ' + statusText +  ']', false);
                    bsAnalytics.trackException('SearchWithDescription: ' + data, false);
                    $dialogs.showAlert('Ocorreu um erro. Status: ' + status, 'Contate o administrador do sistema');
                }
            )
            .finally($ionicLoading.hide);
        }

        function finalizarBusca(query, produto) {
            $rootScope.query = '';
            $rootScope.buscar = false;

            salvarHistorico(query, produto);
        }

        function salvarHistorico(query, produto) {
            if (!naoSalvarHistoricoConsulta) {
                HistoricoConsultasLocalService.salvar(query, produto);
            }
        }

        return service;
    }
})();
