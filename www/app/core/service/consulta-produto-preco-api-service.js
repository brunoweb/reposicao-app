(function() {
    'use strict';

    angular.module('bluesoft-app').factory('ConsultaProdutoPrecoApiService', ConsultaProdutoPrecoApiService);

    ConsultaProdutoPrecoApiService.$inject = ['UserInfo', 'bsRequest'];

    function ConsultaProdutoPrecoApiService(UserInfo, bsRequest) {
        var baseUrl = '/api/consulta-produto-app/preco',
        service = {
            permissoes: {
                listar: listarPermissoes
            },
            preco: {
                salvar: salvarPreco,
                informacoes: obterInformacoes
            }
        };

        return service;

        function listarPermissoes() {
            return bsRequest.get({
                url: getUrl('/permissoes')
            });
        }

        function salvarPreco(params) {
            return bsRequest.post({
                url: getUrl(''),
                data: params
            });
        }

        function obterInformacoes(params) {
            return bsRequest.get({
                url: getUrl('/informacoes'),
                params: params
            });
        }

        function getUrl(url) {
            return UserInfo.usuario.urlTenant + baseUrl + url;
        }
    }
})();
