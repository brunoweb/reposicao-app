(function() {
    'use strict';

    angular.module('bluesoft-app').directive('bsHeaderMenu', BsHeaderMenu);

    BsHeaderMenu.$inject = [];

    function BsHeaderMenu() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/core/directive/bs-header-menu/bs-header-menu-template.html',
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                home: '@',
                escanear: '@',
                historico: '@'
            }
        };

        return directive;
    }

    controller.$inject = ['$state','ConsultaProdutoService','bsHeaderMenuService','bsAnalytics'];

    function controller($state, ConsultaProdutoService, bsHeaderMenuService, bsAnalytics) {
        /* jshint validthis: true */
        var vm = this;

        vm.home = function home() {
            bsAnalytics.trackEvent('Navigation', 'Home', 'Header Menu: Home', 1);
            bsHeaderMenuService.goHome();
        };

        vm.escanearCodigoBarra = function escanearCodigoBarra() {
            bsAnalytics.trackEvent('Search', 'BarCodeScanner', 'Header Menu: Open Camera', 1);
            ConsultaProdutoService.escanearCodigoBarra();
        };

        vm.historico = function historico() {
            bsAnalytics.trackEvent('Navigation', 'History', 'Header Menu: History', 1);
            bsHeaderMenuService.goHistorico();
        };

        return vm;
    }
})();
