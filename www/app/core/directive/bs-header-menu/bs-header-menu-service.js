(function() {
    'use strict';

    angular.module('bluesoft-app').factory('bsHeaderMenuService', BsHeaderMenuService);

    BsHeaderMenuService.$inject = ['$state'];

    function BsHeaderMenuService($state) {
        var service = {
            goHome: goHome,
            goHistorico: goHistorico
        };

        return service;

        function goHome() {
            $state.go('menu.home');
        }

        function goHistorico() {
            $state.go('menu.historico-consultas');
        }
    }
})();
