(function() {
    'use strict';
    // jscs: disable
    angular.module('bluesoft-app')
    .constant('ApiEndpoint',
    {
        // url: 'http://localhost:8100/erp-web', // DEV-ERP-WEB (LOCALHOST)
        // url: 'http://acceptance-teste.interno.bluesoft.com.br:32770/erp-web'
        // url: 'http://localhost:8100/{0}', // DEV  (REMOTE)
        // url: 'http://localhost:8100/beta', // DEV-BETA
        // url: 'http://192.168.253.196:8080/erp-web', // MOBILE (LOCALHOST)
        // url: 'https://erp-2a.bluesoft.com.br/beta', // MOBILE (BETA)
        url: 'https://erp-2a.bluesoft.com.br/{0}', // MOBILE (PRODUCAO)
    });
    // jscs: enable
})();
