(function() {
    'use strict';

    angular.module('bluesoft-app').constant('AppInfo', {
        id: 'br.com.bluesoft.consultadeprodutos'
    });
})();
