(function() {
    'use strict';

    angular.module('bluesoft-app').controller('ConsultaProdutoController', ConsultaProdutoController);

    ConsultaProdutoController.$inject = ['$rootScope','$ionicPopover','$state','$localstorage','$dialogs','$ionicHistory','bsSQlite','UserInfo','ConsultaProdutoService','LoginService','HistoricoConsultasLocalService','bsAnalytics'];

    function ConsultaProdutoController($rootScope,$ionicPopover,$state,$localstorage,$dialogs,$ionicHistory,bsSQlite,UserInfo,ConsultaProdutoService,LoginService,HistoricoConsultasLocalService,bsAnalytics) {
        var vm = this,
        _popoverScope = $rootScope.$new();

        // private
        function sair() {
            $dialogs.show({
                title: 'Sair?',
                template: '<p>Deseja desconectar do seu usuário?</p>',
                onConfirm: function() {
                    bsSQlite.dropAll().then(function() {
                        bsAnalytics.trackEvent('Logout', 'Logout', UserInfo.usuario.id, 1);
                        UserInfo.usuario = null;
                        $ionicHistory.clearHistory();
                        _popoverScope.popover.hide();
                        $state.go('login-tenant');
                    });
                }
            });
        }

        // public
        $ionicPopover.fromTemplateUrl('app/features/consulta-produto/template/usuario-popover-template.html', {
            scope: _popoverScope
        }).then(function(popover) {
            $rootScope.menu = undefined;
            $rootScope.usuario = UserInfo.usuario;
            $rootScope.buscar = false;
            $rootScope.possuiHistoricoConsultas = false;
            HistoricoConsultasLocalService.count().then(function(quantidade) {
                $rootScope.popover = popover;
                $rootScope.sair = sair;
                $rootScope.possuiHistoricoConsultas = quantidade.message ? false : true;
            });
        });

        vm.buscarClick = function buscarClick() {
            bsAnalytics.trackEvent('Search', 'Search', vm.query, 1);
            if (vm.query && vm.query.length > 0) {
                ConsultaProdutoService.buscar(vm.query);
            } else {
                ConsultaProdutoService.escanearCodigoBarra();
            }

            vm.query = '';
        };

        // init
        function init() {
            vm.query = '';
            bsAnalytics.trackView('ConsultaProduto');
        }

        init();
    }
})();
