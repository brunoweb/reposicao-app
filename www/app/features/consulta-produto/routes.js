(function() {
    'use strict';

    angular.module('bluesoft-app').config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {
        $stateProvider
        .state('menu.home', {
            url: '/home',
            views: {
                menuContent: {
                    templateUrl: 'app/features/consulta-produto/template/consulta-produto-template.html',
                    controller: 'ConsultaProdutoController as vm'
                }
            },
            cache: false
        });
    }
})();
