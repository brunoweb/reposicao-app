(function() {
    'use strict';

    angular.module('bluesoft-app').config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {

        $stateProvider
        .state('menu.editar-preco', {
            url: '/editar-preco',
            views: {
                menuContent: {
                    templateUrl: 'app/features/editar-preco/template/template.html',
                    controller: 'EditarPrecoController as vm'
                }
            },
            params: { produto: null, informacoesLoja: null},
            cache: false
        });

    }
})();
