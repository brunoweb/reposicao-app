(function() {
    'use strict';

    angular.module('bluesoft-app').controller('EditarPrecoController', EditarPrecoController);

    EditarPrecoController.$inject = ['$rootScope','$scope','$stateParams','$ionicHistory','$ionicPopup','ConsultaProdutoPrecoApiService','bsAnalytics'];

    function EditarPrecoController($rootScope,$scope,$stateParams,$ionicHistory,$ionicPopup,ConsultaProdutoPrecoApiService,bsAnalytics) {
        var vm = this,
            aplicarPrecoPopupInfo,
            confirmPopupInfo,
            aplicarPrecoPopup;

        vm.voltar = function voltar() {
            $ionicHistory.goBack();
        };

        vm.aplicarPreco = function aplicarPreco(campoPreco) {
            var valorPreco = vm.informacoes[campoPreco];
            if (valorPreco <= 0) {
                return;
            }

            bsAnalytics.trackEvent('Update', 'ApplyPrice', campoPreco, 1);
            $scope.valorPreco = valorPreco;
            aplicarPrecoPopupInfo.title = 'Aplicar <u><b>R$ ' + parseFloat(valorPreco).toFixed(2) + '</b></u> para';
            aplicarPrecoPopup = $ionicPopup.show(aplicarPrecoPopupInfo);
        };

        vm.salvar = function salvar() {
            bsAnalytics.trackEvent('Save', 'SavePrice', vm.produto.produtoKey, 1);

            confirmPopupInfo.template = 'Deseja salvar os preços a vigorar em <b>' + moment(vm.dataInicial).format('DD/MM/YYYY') + '</b>?';
            var confirmPopup = $ionicPopup.confirm(confirmPopupInfo);

            confirmPopup.then(function(res) {
                if (res) {
                    doSalvar();
                }
            });
        };

        vm.recalcularMargens = recalcularMargens;

        // private

        function doSalvar() {
            if (!validarPreco()) {
                return;
            }

            var params = {
                produtoKey: vm.produto.produtoKey,
                preco: vm.preco,
                precoFidelidade: vm.precoFidelidade,
                dataInicial: moment(vm.dataInicial).format('DD/MM/YYYY')
            };

            if (vm.informacoes.regiaoPrecoKey) {
                params.regiaoPrecoKey = vm.informacoes.regiaoPrecoKey;
            } else {
                params.lojaKey = vm.informacoes.lojaKey;
            }

            ConsultaProdutoPrecoApiService.preco.salvar(params).then(function() {
                vm.informacoesLoja.preco = vm.preco;
                vm.informacoesLoja.precoFidelidade = vm.precoFidelidade;
                vm.informacoesLoja.margemBruta = vm.margemBruta;
                vm.informacoesLoja.margemLiquida = vm.margemLiquida;

                $ionicPopup.alert({
                    title: 'Alteração de Preço',
                    template: 'Preço salvo com sucesso',
                    cssClass: 'view-editar-preco-confirm-popup'
                });
            });
        }

        function validarPreco() {
            if (vm.preco <= 0) {
                var mensagem = vm.preco === 0 ? 'Preço não pode ser zero' : 'Preço não pode ser menor que zero';

                $ionicPopup.alert({
                    title: 'Preço Inválido',
                    template: mensagem,
                    cssClass: 'view-editar-preco-confirm-popup'
                });

                return false;
            }

            return true;
        }

        function recalcularMargens() {
            vm.margemBruta = ((vm.preco - vm.informacoesLoja.custo) / vm.preco) * 100;
            vm.margemLiquida = ((vm.preco - vm.informacoesLoja.valorTributacaoVenda - vm.informacoesLoja.custo) / vm.preco) * 100;

        }

        function aplicarPrecoPeloPopup(campoPreco, valorPreco) {
            bsAnalytics.trackEvent('Confirm', 'ApplyPriceConfirm', campoPreco, 1);

            vm[campoPreco] = valorPreco;
            recalcularMargens();
            aplicarPrecoPopup.close();
        }

        function obterInformacoesPreco() {
            var params = {
                produtoKey: vm.produto.produtoKey,
                lojaKey: vm.informacoesLoja.lojaKey
            };

            ConsultaProdutoPrecoApiService.preco.informacoes(params).then(function(res) {
                if (res.data && res.data.length === 1) {
                    vm.informacoes = res.data[0];
                }
            });
        }

        function initPopups() {
            $scope.aplicarPreco = aplicarPrecoPeloPopup;

            aplicarPrecoPopupInfo = {
                templateUrl: 'app/features/editar-preco/template/aplicar-preco-popup-template.html',
                scope: $scope,
                cssClass: 'view-editar-preco-popup',
                buttons: [
                    { text: 'Cancelar' }
                ]
            };

            confirmPopupInfo = {
                title: 'Confirmar',
                cssClass: 'view-editar-preco-confirm-popup',
                cancelText: 'Cancelar',
                okText: 'Confirmar'
            };
        }

        function init() {
            bsAnalytics.trackView('EditarPreco');

            $rootScope.headerWithoutMenu = true;
            $rootScope.withoutHeader = true;
            $rootScope.menu = {};

            vm.produto = $stateParams.produto;
            vm.informacoesLoja = $stateParams.informacoesLoja;
            vm.preco = vm.informacoesLoja.preco;
            vm.precoFidelidade = vm.informacoesLoja.precoFidelidade;
            vm.margemBruta = vm.informacoesLoja.margemBruta;
            vm.margemLiquida = vm.informacoesLoja.margemLiquida;
            vm.dataInicial = moment().add(1, 'days').toDate();

            initPopups();
            obterInformacoesPreco();
        }

        init();
    }
})();
