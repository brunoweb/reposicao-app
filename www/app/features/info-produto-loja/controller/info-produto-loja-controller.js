(function() {
    'use strict';

    angular.module('bluesoft-app').controller('InfoProdutoLojaController', InfoProdutoLojaController);

    InfoProdutoLojaController.$inject = ['$rootScope', '$state', '$stateParams', '$ionicHistory','bsAnalytics'];

    function InfoProdutoLojaController($rootScope, $state, $stateParams, $ionicHistory,bsAnalytics) {

        var vm = this;

        vm.detalharLoja = function detalharLoja(produto, informacoesLoja) {
            bsAnalytics.trackEvent('Show', 'ShowStoreInfo', 'Show store info: ' + produto.produtoKey, 1);

            $state.go('menu.info-produto-loja-detalhe', {
                produto: produto,
                informacoesLoja: informacoesLoja
            });
        };

        vm.voltar = function voltar() {
            $ionicHistory.goBack();
        };

        // private

        function init() {
            bsAnalytics.trackView('InfoProdutoLoja');

            vm.produto = $stateParams.produto;

            $rootScope.showMenu = true;
            $rootScope.menu = {
                historico: true,
                escanear: true
            };
            $rootScope.headerWithoutMenu = false;
            $rootScope.withoutHeader = false;
        }

        init();
    }
})();
