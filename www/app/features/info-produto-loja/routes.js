(function() {
    'use strict';

    angular.module('bluesoft-app').config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {

        $stateProvider
        .state('menu.info-produto-loja', {
            url: '/info-produto-loja',
            views: {
                menuContent: {
                    templateUrl: 'app/features/info-produto-loja/template/info-produto-loja-template.html',
                    controller: 'InfoProdutoLojaController as vm'
                }
            },
            params: { produto: null },
            cache: false
        });

    }
})();
