(function() {
    'use strict';

    angular.module('bluesoft-app').factory('HistoricoConsultasLocalService', HistoricoConsultasLocalService);

    HistoricoConsultasLocalService.$inject = ['$cordovaSQLite', 'UserInfo', '_'];

    function HistoricoConsultasLocalService($cordovaSQLite, UserInfo, _) {
        var service = {
            salvar: salvar,
            listar: listar,
            count: count
        },

        queryTextSearch = '',
        descricao = null;

        function salvar(queryText, data) {
            init();
            queryTextSearch = queryText;
            if (data) {
                descricao = data.descricao;
            }

            obterPorQuery(queryText).then(function(data) {
                if (data) {
                    deletarPorQuery(queryText).then(doSalvar);
                } else {
                    deletarConsultaAntigaFatorDez();
                    doSalvar();
                }
            });
        }

        function doSalvar() {
            var query = 'INSERT INTO HISTORICO_CONSULTAS ' +
            '( query , ' +
            'produtoDescricao , ' +
            'data , ' +
            'usuario) ' +
            ' values(?, ?, ?, ?)',

            data = [
                queryTextSearch,
                descricao,
                new Date(),
                UserInfo.usuario.id
            ];

            $cordovaSQLite.execute(SQLiteDb, query, data).then(function(response) {
                queryTextSearch = '';
                descricao = null;
                return response.data;
            }, function(err) {
                return err;
            });
        }

        function deletarPorQuery(queryText) {
            var query = 'DELETE FROM HISTORICO_CONSULTAS where query = ? and usuario = ?';

            return $cordovaSQLite.execute(SQLiteDb, query, [queryText, UserInfo.usuario.id]).then(null);
        }

        function deletarConsultaAntigaFatorDez() {
            count().then(function(data) {
                if (data.quantidade == 10) {
                    var query = 'DELETE FROM HISTORICO_CONSULTAS where query = (SELECT QUERY FROM HISTORICO_CONSULTAS LIMIT 1) AND usuario = ?';

                    return $cordovaSQLite.execute(SQLiteDb, query, [UserInfo.usuario.id]).then(null);
                }
            });
        }

        function listar() {
            var query = 'SELECT * FROM HISTORICO_CONSULTAS WHERE usuario = ?';

            return $cordovaSQLite.execute(SQLiteDb, query, [UserInfo.usuario.id]).then(function(response) {
                if (response.rows.length > 0) {
                    var rows = obterRows(response.rows);
                    _.each(rows, function(row) {
                        row.dataObject = new Date(row.data);
                    });

                    return _.sortBy(rows, 'dataObject').reverse();
                }
            }, function(err) {
                return err;
            });
        }

        function obterPorQuery(queryText) {
            var query = 'SELECT * FROM HISTORICO_CONSULTAS where query = ? and usuario = ?';

            return $cordovaSQLite.execute(SQLiteDb, query, [queryText, UserInfo.usuario.id]).then(function(response) {
                if (response.rows.length > 0) {
                    return obterRows(response.rows)[0];
                }
            }, function(err) {
                return err;
            });
        }

        function count() {
            var query = 'SELECT COUNT(*) quantidade FROM HISTORICO_CONSULTAS WHERE usuario = ?';

            return $cordovaSQLite.execute(SQLiteDb, query, [UserInfo.usuario.id]).then(function(response) {
                if (response.rows.length > 0) {
                    return obterRows(response.rows)[0];
                }
            }, function(err) {
                return err;
            });
        }

        function obterRows(rows) {
            var i = 0,
            length = rows.length,
            response = [];

            for (i; i < length; i++) {
                response.push(rows.item(i));
            }

            return response;
        }

        function init() {
            /* jscs:disable */
            var DDL = 'CREATE TABLE IF NOT EXISTS HISTORICO_CONSULTAS ( ' +
            'query integer, ' +
            'produtoDescricao text, ' +
            'data date, ' +
            'usuario integer )';

            $cordovaSQLite.execute(SQLiteDb, DDL).then(function() {});
            /* jscs:enable */
        }

        return service;
    }
})();
