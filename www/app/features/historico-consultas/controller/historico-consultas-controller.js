(function() {
    'use strict';

    angular.module('bluesoft-app').controller('HistoricoConsultasController', HistoricoConsultasController);

    HistoricoConsultasController.$inject = ['$rootScope', 'HistoricoConsultasLocalService', 'ConsultaProdutoService','bsAnalytics'];

    function HistoricoConsultasController($rootScope, HistoricoConsultasLocalService, ConsultaProdutoService,bsAnalytics) {
        var vm = this;

        vm.buscar = function(query) {
            bsAnalytics.trackEvent('Search', 'SearchWithHistory', 'Search with history: ' + query, 1);
            ConsultaProdutoService.buscar(query);
        };

        vm.convertToDate = function(stringDate) {
            return new Date(stringDate);
        };

        // private
        function init() {
            bsAnalytics.trackView('HistoricoConsultas');

            HistoricoConsultasLocalService.listar().then(function(data) {
                vm.historicoConsultas = data;
            });

            $rootScope.headerWithoutMenu = false;
            $rootScope.menu = {
                historico: false,
                escanear: true
            };

        }

        init();
    }
})();
