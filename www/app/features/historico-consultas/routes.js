(function() {
    'use strict';

    angular.module('bluesoft-app').config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {

        $stateProvider
        .state('menu.historico-consultas', {
            url: '/historico-consultas',
            views: {
                menuContent: {
                    templateUrl: 'app/features/historico-consultas/template/historico-consultas-template.html',
                    controller: 'HistoricoConsultasController as vm'
                }
            },
            cache: false
        });

    }
})();
