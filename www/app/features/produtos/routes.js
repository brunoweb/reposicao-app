(function() {
    'use strict';

    angular.module('bluesoft-app')
    .config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {

        $stateProvider
        .state('menu.produtos', {
            url: '/produtos',
            views: {
                menuContent: {
                    templateUrl: 'app/features/produtos/template/produtos-template.html',
                    controller: 'ProdutosController as vm'
                }
            },
            params: { descricao: null, produtos: null },
            cache: false
        });

    }
})();
