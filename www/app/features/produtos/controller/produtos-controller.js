(function() {
    'use strict';

    angular.module('bluesoft-app').controller('ProdutosController', ProdutosController);

    ProdutosController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '_', 'ConsultaProdutoService','bsAnalytics'];

    function ProdutosController($scope, $rootScope, $state, $stateParams, _, ConsultaProdutoService,bsAnalytics) {

        var vm = this;

        vm.detalharProduto = function detalharProduto(produto) {
            bsAnalytics.trackEvent('Show', 'ShowProductStores', 'Show product stores: ' + produto.produtoKey, 1);
            ConsultaProdutoService.buscar(produto.gtin);
        };

        // private

        function init() {
            bsAnalytics.trackView('EditarPreco');

            vm.descricao = $stateParams.descricao;
            vm.produtos = $stateParams.produtos;

            $rootScope.headerWithoutMenu = false;
            $rootScope.menu = {
                escanear: true,
                historico: true
            };
        }

        $scope.$on('$ionicView.loaded', function() {
            ConsultaProdutoService.buscarFotos(vm.produtos);
        });

        init();
    }
})();
