(function() {
    'use strict';

    angular.module('bluesoft-app').controller('InfoProdutoLojaDetalheController', InfoProdutoLojaDetalheController);

    InfoProdutoLojaDetalheController.$inject = ['$stateParams','$state','$rootScope','$ionicHistory','ConsultaProdutoPrecoApiService','bsAnalytics'];

    function InfoProdutoLojaDetalheController($stateParams,$state,$rootScope,$ionicHistory,ConsultaProdutoPrecoApiService,bsAnalytics) {
        var vm = this;

        vm.estoqueTotal = function estoqueTotal(informacoesLoja) {
            return ((informacoesLoja.estoqueTotal || 0) +
            (informacoesLoja.estoqueReservado || 0) +
            (informacoesLoja.estoqueEmTerceiros || 0) +
            (informacoesLoja.estoqueFuturo || 0));
        };

        vm.voltar = function voltar() {
            $ionicHistory.goBack();
        };

        vm.editarPreco = function editarPreco() {
            if (!vm.permissoes.podeEditarPreco) {
                return;
            }

            bsAnalytics.trackEvent('Show', 'ShowPriceInfo', 'Show price info: ' + vm.produto.produtoKey, 1);
            $state.go('menu.editar-preco', {
                produto: vm.produto,
                informacoesLoja: vm.informacoesLoja
            });
        };

        // private

        function initPermissoesPreco() {
            vm.permissoes = {};
            ConsultaProdutoPrecoApiService.permissoes.listar().then(function listarPermissoes(res) {
                vm.permissoes = res.data;
                vm.permissoes.podeEditarPreco = vm.permissoes.precificacao &&
                vm.permissoes.precificacaoProdutos &&
                vm.permissoes.incluir &&
                vm.permissoes.alterar;
            });
        }

        function init() {
            bsAnalytics.trackView('InfoProdutoLojaDetalhe');

            vm.produto = $stateParams.produto;
            vm.informacoesLoja = $stateParams.informacoesLoja;

            $rootScope.showMenu = true;
            $rootScope.menu = {
                historico: true,
                escanear: true
            };
            $rootScope.headerWithoutMenu = false;
            $rootScope.withoutHeader = false;

            initPermissoesPreco();
        }

        init();
    }
})();
