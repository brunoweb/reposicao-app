(function() {
    'use strict';

    angular.module('bluesoft-app').config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {

        $stateProvider
        .state('menu.info-produto-loja-detalhe', {
            url: '/info-produto-loja-detalhe',
            views: {
                menuContent: {
                    templateUrl: 'app/features/info-produto-loja-detalhe/template/info-produto-loja-detalhe-template.html',
                    controller: 'InfoProdutoLojaDetalheController as vm'
                }
            },
            params: { produto: null, informacoesLoja: null},
            cache: false
        });
    }
})();
