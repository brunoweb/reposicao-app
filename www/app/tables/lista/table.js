(function() {
    'use strict';

    angular.module('bluesoft-app').service('ListaLocal', ListaLocal);

    ListaLocal.$inject = ['bsSQlite'];

    function ListaLocal(bsSQlite) {
        var service = {
                table: 'ITENS',
                salvar: salvar,
                listar: listar,
                remover: remover
            },
            schema = {
                id: {
                    type: 'integer',
                    primary: true
                },
                descricaoGondola: {
                    type: 'text'
                },
                gtin: {
                    type: 'text'
                },
                img: {
                    type: 'text'
                }
            };

        return service;

        function create() {
            return bsSQlite.createFromSchema(service.table, schema);
        }

        function salvar(item) {
            return create().then(function tableCreated() {
                return bsSQlite.insertObj(service.table, {
                    params: item,
                    schema: schema
                });
            });
        }

        function listar() {
            return create().then(function tableCreated() {
                return bsSQlite.select(service.table);
            });
        }

        function remover() {
            return bsSQlite.remove(service.table);
        }
    }
})();
