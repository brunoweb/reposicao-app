(function() {
    'use strict';

    angular.module('bluesoft-app').service('UsuarioLocal', UsuarioLocal);

    UsuarioLocal.$inject = ['bsSQlite'];

    function UsuarioLocal(bsSQlite) {
        var service = {
                table: 'USUARIO',
                salvar: salvar,
                obter: obter,
                remover: remover,
                removerToken: removerToken
            },
            schema = {
                id: {
                    type: 'integer',
                    primary: true
                },
                usuario: {
                    type: 'text'
                },
                nomeAbreviado: {
                    type: 'text'
                },
                tenant: {
                    type: 'text'
                },
                urlTenant: {
                    type: 'text'
                },
                urlLogo: {
                    type: 'text'
                },
                token: {
                    type: 'text'
                },
                uuid: {
                    type: 'text'
                },
                push: {
                    type: 'text'
                }
            };

        return service;

        function create() {
            return bsSQlite.createFromSchema(service.table, schema);
        }

        function salvar(usuario) {
            return create().then(function tableCreated() {
                return bsSQlite.insertObj(service.table, {
                    params: usuario,
                    schema: schema
                });
            });
        }

        function obter() {
            return create().then(function tableCreated() {
                return bsSQlite.selectUnique(service.table);
            });
        }

        function remover() {
            return bsSQlite.remove(service.table);
        }

        function removerToken() {
            return obter().then(function obterUsuario(usuario) {
                usuario.token = null;
                return bsSQlite.update(usuario);
            });
        }
    }
})();
