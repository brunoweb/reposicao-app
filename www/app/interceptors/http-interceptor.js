(function() {
    'use strict';

    angular.module('bluesoft-app').config(configHttpProvider);

    configHttpProvider.$inject = ['$httpProvider'];

    function configHttpProvider($httpProvider) {
        $httpProvider.defaults.withCredentials = true;
        $httpProvider.interceptors.push(bsHttpInterceptor);
    }

    bsHttpInterceptor.$inject = ['$q', '$rootScope', 'UserInfo', '$injector'];

    function bsHttpInterceptor($q, $rootScope, UserInfo, $injector) {
        var interceptor, headerToken;

        interceptor = {
            requests: 0,
            request: function(config) {
                interceptor.requests++;
                if (!config.hideLoader) {
                    $rootScope.$broadcast('loading:show');
                }

                if (UserInfo.usuario) {
                    if (UserInfo.usuario.token && (config.url.indexOf('j_spring_security') == -1)) {
                        headerToken = { 'X-CustomToken': UserInfo.usuario.token};
                        angular.extend(config.headers, headerToken);
                    }
                }

                return config;
            },

            requestError: function(rejection) {
                return $q.reject(rejection);
            },

            response: function(response) {
                interceptor.requests--;
                if (interceptor.requests <= 0) {
                    $rootScope.$broadcast('loading:hide');
                }

                return response || $q.when(response);
            },

            responseError: function(rejection) {
                interceptor.requests--;
                if (interceptor.requests <= 0) {
                    $rootScope.$broadcast('loading:hide');
                }

                // handle para redirect após login/logout no ERP
                if (rejection.config.url.indexOf('j_spring_security') != -1) {
                    return rejection;

                } else if (rejection.status === 401) {
                    return $injector.get('LoginService').logout().then(function(res) {
                        if (res.logout) {
                            $injector.invoke([
                                '$state', 'UserInfo', function($state, UserInfo) {
                                    UserInfo.usuario = null;
                                    return $state.go('login-tenant', {
                                        statusOrigin: {
                                            status: rejection.status
                                        }
                                    });
                                }
                            ]);

                            return $q.reject({
                                data: {
                                    errors: [
                                        {message: 'Sessão expirada'}
                                    ]
                                }
                            });
                        }
                    });

                } else {
                    return $q.reject(rejection);
                }
            }
        };

        return interceptor;
    }
})();
