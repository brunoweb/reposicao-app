(function() {
    'use strict';

    angular.module('bluesoft-app').directive('bsFormatter', bsFormatter);

    bsFormatter.$inject = ['$filter'];

    function bsFormatter($filter) {
        var directive = {
            restrict: 'A',
            require: '?ngModel',
            link: link
        },
        _formatter = {
            currency: 'currency',
            percentage: 'percentage'
        },
        currencyPrefix = 'R$',
        percentageSuffix = '%';

        return directive;

        function link($scope, elem, attrs, vm) {
            var modelValue,
            isCurrency = attrs.bsFormatter == _formatter.currency,
            isPercentage = attrs.bsFormatter == _formatter.percentage;

            if (isCurrency) {
                attrs.prefix = attrs.hasOwnProperty('prefix') ? attrs.prefix : currencyPrefix;
            } else if (isPercentage) {
                attrs.suffix = attrs.hasOwnProperty('suffix') ? attrs.suffix : percentageSuffix;
            }

            if (!vm) {
                return;
            }

            vm.$formatters.unshift(function formattersUnshift() {
                var retorno;

                vm.$modelValue = modelValue ? modelValue : vm.$modelValue;
                modelValue = undefined;

                if (isCurrency) {
                    retorno = $filter('currency')(vm.$modelValue);
                } else if (isPercentage) {
                    retorno = $filter('number')(vm.$modelValue) + percentageSuffix;
                }

                return retorno;
            });

            vm.$parsers.unshift(function parsersUnshift() {
                var $formatter = new PriceFormat({
                    elem: elem[0],
                    prefix: attrs.prefix,
                    suffix: attrs.suffix,
                    allowNegative: attrs.hasOwnProperty('allowNegative')
                });

                vm.$viewValue = elem[0].value;
                modelValue = undefined;

                return Number($formatter.price.integerVal + '.' + $formatter.price.centsVal);
            });
        }
    }
})();
