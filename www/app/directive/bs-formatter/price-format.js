(function() {
    'use strict';

    function PriceFormat() {
        var defaults = {
            prefix: '',
            suffix: '',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: false,
            insertPlusSign: false,
            centsLimit: 2
        },
        options = getConfiguration(defaults, arguments[0]);

        init(options);
    }

    function init(options) {
        var obj = options.elem,
            isNumber = /[0-9]/,
            prefix = options.prefix,
            suffix = options.suffix,
            centsSeparator = options.centsSeparator,
            thousandsSeparator = options.thousandsSeparator,
            centsLimit = options.centsLimit,
            allowNegative = options.allowNegative,
            insertPlusSign = options.insertPlusSign,
            char_;

        if (insertPlusSign) {
            allowNegative = true;
        }

        function toNumbers(str) {
            var formatted = '',
                i = 0,
                length = str ? str.length : 0;

            for (i; i < length; i++) {
                char_ = str.charAt(i);

                if (Number(formatted.length) === 0 && Number(char_) === 0) {
                    char_ = false;
                }

                if (char_ && char_.match(isNumber)) {
                    formatted = formatted + char_;
                }
            }

            return formatted;
        }

        function fillWithZeroes(str) {
            while (str.length < (centsLimit + 1)) {
                str = '0' + str;
            }

            return str;
        }

        function getPriceInfo(str) {
            var formatted = fillWithZeroes(toNumbers(str));

            return {
                formatted: formatted,
                centsVal: formatted.substr(formatted.length - centsLimit, centsLimit),
                integerVal: formatted.substr(0, formatted.length - centsLimit),
                full: Number(formatted.replace(/[^0-9\.-]+/g, ''))
            };
        }

        function priceFormat(str) {
            var price = getPriceInfo(str),
            thousandsFormatted = '',
            thousandsCount = 0,
            j = price.integerVal.length;

            if (centsLimit === 0) {
                centsSeparator = '';
                centsVal = '';
            }

            price.formatted = (centsLimit === 0) ? price.integerVal : price.integerVal + centsSeparator + price.centsVal;

            if (thousandsSeparator || thousandsSeparator.trim() !== '') {
                for (j; j > 0; j--) {
                    char_ = price.integerVal.substr(j - 1, 1);
                    thousandsCount++;

                    if (thousandsCount % 3 === 0) {
                        char_ = thousandsSeparator + char_;
                    }

                    thousandsFormatted = char_ + thousandsFormatted;
                }

                if (thousandsFormatted.substr(0, 1) == thousandsSeparator) {
                    thousandsFormatted = thousandsFormatted.substring(1, thousandsFormatted.length);
                }

                price.formatted = (centsLimit === 0) ? thousandsFormatted : thousandsFormatted + centsSeparator + price.centsVal;
            }

            // if the string contains a dash, it is negative - add it to the begining (except for zero)
            if (allowNegative && (price.integerVal !== 0 || price.centsVal !== 0)) {
                if (str.indexOf('-') != -1 && str.indexOf('+') < str.indexOf('-')) {
                    price.formatted = '-' + price.formatted;
                } else {
                    if (!insertPlusSign) {
                        price.formatted = '' + price.formatted;
                    }
                    else {
                        price.formatted = '+' + formatted;
                    }
                }
            }

            if (prefix) {
                price.formatted = prefix + price.formatted;
            }

            if (suffix) {
                price.formatted = price.formatted + suffix;
            }

            return price;
        }

        function keyCheck(e) {
            var code = (e.keyCode ? e.keyCode : e.which),
            typed = String.fromCharCode(code),
            functional = false,
            str = obj.value,
            newValue = priceFormat(str + typed);

            if ((code >= 48 && code <= 57) || (code >= 96 && code <= 105)) functional = true;
            if (code === 8) functional = true;
            if (code === 9) functional = true;
            if (code === 13) functional = true;
            if (code === 46) functional = true;
            if (code === 37) functional = true;
            if (code === 39) functional = true;
            if (allowNegative && (code == 189 || code == 109 || code == 173)) functional = true;
            if (insertPlusSign && (code == 187 || code == 107 || code == 61)) functional = true;

            if (!functional) {
                e.preventDefault();
                e.stopPropagation();

                if (str !== newValue) {
                    obj.value = newValue;
                }
            }
        }

        function priceIt() {
            var str = obj.value,
            price = priceFormat(str);

            if (str !== price) {
                obj.value = price.formatted;
            }

            PriceFormat.prototype.price = price;
        }

        options.elem.addEventListener('keydown.priceFormat', keyCheck);
        options.elem.addEventListener('keyup.priceFormat', priceIt);
        options.elem.addEventListener('focusout.priceFormat', priceIt);

        if (options.elem.value.length > 0) {
            priceIt();
        }
    }

    function getConfiguration(source, properties) {
        for (var prop in properties) {
            if (properties.hasOwnProperty(prop)) {
                source[prop] = properties[prop];
            }
        }

        return source;
    }

    window.PriceFormat = PriceFormat;
})();
