(function() {
    'use strict';

    angular.module('bluesoft-app')
    .controller('MenuController', MenuController);

    MenuController.$inject = ['ConsultaProdutoService'];

    function MenuController(ConsultaProdutoService) {
        var vm = this;

        vm.buscar = function buscar(query) {
            ConsultaProdutoService.buscar(query);
        };

        // private

        function init() {
        }

        init();
    }
})();
