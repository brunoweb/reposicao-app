(function() {
    'use strict';

    angular.module('bluesoft-app')
    .config(routes)
    .run(['$rootScope', '$state', '$ionicNavBarDelegate', 'LoginService', 'UserInfo', function($rootScope, $state, $ionicNavBarDelegate, LoginService, UserInfo) {
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            console.log(error);
            console.log(error.stack);
            $state.go('menu.home');
        });

        $rootScope.$on('$stateChangeSuccess', function() {
            $rootScope.$on('$ionicView.enter', function() {
                $ionicNavBarDelegate.showBar(true);
            });
        });

        $rootScope.$on('$stateChangeStart', function(event, toState) {
            if (!toState.loginPage && !UserInfo.usuario) {
                event.preventDefault();
                $state.go('login-tenant');
            } else if (toState.loginPage && UserInfo.usuario) {
                event.preventDefault();
                $state.go('menu.home');
            }
        });

    }]);

    routes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routes($stateProvider, $urlRouterProvider) {
        $stateProvider.state('login-tenant', {
            url: '/login-tenant',
            templateUrl: 'app/commons/login/template/login-tenant-template.html',
            controller: 'LoginTenantController as vm',
            loginPage: true
        }).state('login-usuario', {
            url: '/login-usuario/:tenant',
            templateUrl: 'app/commons/login/template/login-usuario-template.html',
            controller: 'LoginUsuarioController as vm',
            cache: false,
            loginPage: true
        }).state('lista', {
            url: '/lista',
            controller: 'ListaController as vm',
            templateUrl: 'app/lista/template/lista-template.html'
        }).state('menu', {
            url: '/menu',
            abstract: true,
            templateUrl: 'app/menu/template/menu-template.html'
        });

        $urlRouterProvider.otherwise(function($injector) {
            var $state = $injector.get('$state');
            return $state.go('login-tenant');
        });
    }
})();
