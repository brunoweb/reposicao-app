(function() {
    'use strict';

    angular.module('bluesoft-app')
        .run(runCordova)
        .run(runSpinnerEvent)
        .config(configBackButton);

    runCordova.$inject = ['$rootScope', '$ionicPlatform', 'UserInfo', 'ApiEndpoint', 'bsAnalytics'];

    function runCordova($rootScope, $ionicPlatform, UserInfo, ApiEndpoint, bsAnalytics) {
        $ionicPlatform.ready(function() {
            if (UserInfo.usuario) {
                UserInfo.usuario.urlTenant = ApiEndpoint.url.replace('{0}', UserInfo.usuario.tenant);
            }

            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.disableScroll(true);
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }

            if (window.StatusBar) {
                StatusBar.styleLightContent();
            }

            bsAnalytics.startTrackerWithId('UA-6234192-26');
        });
    }

    runSpinnerEvent.$inject = ['$rootScope', '$ionicLoading'];

    function runSpinnerEvent($rootScope, $ionicLoading) {
        $rootScope.$on('loading:show', function() {
            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner> Carregando...'
            });
        });

        $rootScope.$on('loading:hide', function() {
            $ionicLoading.hide();
        });
    }

    configBackButton.$inject = ['$ionicConfigProvider'];

    function configBackButton($ionicConfigProvider) {
        $ionicConfigProvider.backButton.previousTitleText(false).text('');
        $ionicConfigProvider.scrolling.jsScrolling(false);
        $ionicConfigProvider.navBar.alignTitle('center');
    }

    //bootstrap manual do angular
    window.ionic.Platform.ready(checkUser);

    function checkUser() {
        var service, injector = angular.injector(['bluesoft-sqlite']);
        service = injector.get('bsSQlite');
        service.selectUnique('USUARIO').then(bootstrap, bootstrapWithoutUser);
    }

    function bootstrap(usuario) {
        window.bluesoftApp.value('UserInfo', {
            usuario: usuario
        });
        return angular.bootstrap(document, ['bluesoft-app']);
    }

    function bootstrapWithoutUser() {
        return bootstrap();
    }
})();
