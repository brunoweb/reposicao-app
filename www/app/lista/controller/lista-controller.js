(function() {
    'use strict';

    angular.module('bluesoft-app')
    .controller('ListaController', ListaController);

    ListaController.$inject = ['bsSQlite', 'ListaLocal'];
    function ListaController(bsSQlite, ListaLocal) {
        var vm = this;

        vm.excluir = function($index, id) {
            db.execute('DELETE FROM items WHERE id = ?', [id]).then(function() {
                vm.items.splice($index, 1);
            });
        };

        function init() {
            ListaLocal.listar().then(function(data) {
                vm.items = data;
            });
        }

        init();
    }
})();
