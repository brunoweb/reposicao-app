(function() {
    'use strict';

    angular.module('bluesoft-app').service('bsAnalytics', bsAnalytics);

    bsAnalytics.$inject = ['$cordovaGoogleAnalytics'];
    function bsAnalytics($cordovaGoogleAnalytics) {
        var service = {
            startTrackerWithId: startTrackerWithId,
            setUserId: setUserId,
            setAppVersion: setAppVersion,
            trackView: trackView,
            trackEvent: trackEvent,
            trackException: trackException,
            trackMetric: trackMetric
        };

        return service;

        function startTrackerWithId(id) {
            if (isActive()) {
                $cordovaGoogleAnalytics.startTrackerWithId(id);
            }
        }

        function setUserId(userId) {
            if (isActive()) {
                $cordovaGoogleAnalytics.setUserId(userId);
            }
        }

        function setAppVersion(appVersion) {
            if (isActive()) {
                $cordovaGoogleAnalytics.setAppVersion(appVersion);
            }
        }

        function trackView(title) {
            if (isActive()) {
                $cordovaGoogleAnalytics.trackView(title);
            }
        }

        function trackEvent(category, action, label, value) {
            if (isActive()) {
                $cordovaGoogleAnalytics.trackEvent(category, action, label, value);
            }
        }

        function trackException(description, fatal) {
            if (isActive()) {
                $cordovaGoogleAnalytics.trackEvent(description, fatal);
            }
        }

        function trackMetric(key, value) {
            if (isActive()) {
                $cordovaGoogleAnalytics.trackMetric(key, value);
            }
        }

        function isActive() {
            return window.cordova;
        }
    }
})();
