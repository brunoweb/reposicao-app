(function() {
    'use strict';

    angular.module('bluesoft-app').service('bsRequest', bsRequest);

    bsRequest.$inject = ['$http', '$q', '$ionicPopup', '$cordovaNetwork', '$timeout'];
    function bsRequest($http, $q, $ionicPopup, $cordovaNetwork, $timeout) {
        var service;

        service = {
            /**
            * @name bsRequestService#post
            * @param  {Object} config Configuração usada para o request
            * @returns {Promise} Retorna promise da requisição
            *
            * @description
            * Faz chamada POST para o servidor
            */
            post: function(config) {
                var reqConfig, request;
                reqConfig = {
                    method: 'POST'
                };
                request = prepareRequest(angular.extend(config, reqConfig));
                return request;
            },

            /**
            * @name bsRequestService#put
            * @param  {Object} config Configuração usada para o request
            * @returns {Promise} Retorna promise da requisição
            *
            * @description
            * Faz chamada PUT para o servidor
            */
            put: function(config) {
                var reqConfig, request;
                reqConfig = {
                    method: 'PUT'
                };
                request = prepareRequest(angular.extend(config, reqConfig));
                return request;
            },

            /**
            * @name bsRequestService#get
            * @param  {Object} config Configuração usada para o request
            * @returns {Promise} Retorna promise da requisição
            *
            * @description
            * Faz chamada GET para o servidor
            */
            get: function(config) {
                var reqConfig, request;
                reqConfig = {
                    method: 'GET'
                };
                request = prepareRequest(angular.extend(config, reqConfig));
                return request;
            },

            /**
            * @name bsRequestService#remove
            * @param  {Object} config Configuração usada para o request
            * @returns {Promise} Retorna promise da requisição
            *
            * @description
            * Faz chamada DELETE para o servidor
            */
            remove: function(config) {
                var reqConfig, request;
                reqConfig = {
                    method: 'DELETE',
                    confirm: true
                };
                request = prepareRequest(angular.extend(config, reqConfig));
                return request;
            }
        };

        return service;

        // privates

        /**
        * @name prepareRequest
        * @param  {Object} config Configuração usada para o request
        * @returns {Promise} Retorna promise da requisição
        *
        * @description
        * Verifica se precisa de confirmação para continuar com o request, e também trata corretamente funções caso
        * confirmação seja negada rejeitando a Promise criada com a mensagem 'canceled'
        */
        function prepareRequest(config) {
            var defer = $q.defer();

            if (config.confirm) {
                bsAlert.confirm({
                    closeOnConfirm: true,
                    html: true,
                    text: config.confirmMessage || 'Deseja realmente executar esta ação?'
                }, function(canContinue) {
                    confirmRequest(config, canContinue, defer);
                });
            } else {
                confirmRequest(config, true, defer);
            }

            return defer.promise;
        }

        /**
        * @name confirmRequest
        * @param  {Object} config Configuração usada para o request
        * @param  {Boolean} canContinue Confirmação para executar o request ou onCancel
        * @param  {Object} defer Promise para ser usada para rejeitar requisição corretamente
        *
        * @description
        * Verifica se precisa de confirmação para continuar com o request, e também trata corretamente funções caso
        * confirmação seja negada rejeitando a Promise criada com a mensagem 'canceled'
        */
        function confirmRequest(config, canContinue, defer) {
            var request;

            if (canContinue) {
                request = sendRequest(config);
                request.then(defer.resolve, defer.reject);
            } else {
                if (typeof config.onCancel === 'function') {
                    config.onCancel();
                }

                defer.reject('canceled');
            }
        }

        /**
        * @name sendRequest
        * @param  {Object} config Configuração para o request
        *
        * @description
        * Executa o request para o servidor, e chama a função padrão para
        * cada etapa da promisse
        */
        function sendRequest(config) {
            if (!window.cordova || $cordovaNetwork.isOnline()) {
                return doServerRequest(config);
            } else {
                return rejectOffline();
            }
        }

        function rejectOffline() {
            var defer = $q.defer();

            $timeout(function() {
                rejectedRequest({
                    data: {
                        reason: 'Sem conexão com a Internet'
                    }
                });
                defer.reject();
            }, 1000);

            return defer.promise;
        }

        function doServerRequest(config) {
            var request;

            request = $http(config);
            request.then(function() {
                return resolvedRequest(config.successMessage);
            });

            if (!config.hideErrorMessage) {
                request.catch(rejectedRequest);
            }

            return request;
        }

        /**
        * @name resolvedRequest
        * @param  {String} msg Mensagem recebida nas configurações
        *
        * @description
        * Exibe mensagem de sucesso caso necessário
        */
        function resolvedRequest(msg) {
            if (typeof msg !== 'undefined' && msg !== null) {
                $ionicPopup.alert({
                    title: msg
                });
            }
        }

        /**
        * @name concatErrorsMessage
        * @param  {Object} res Resposta recebida do servidor
        *
        * @description
        * Concatena mensagens de erro recebidas (em lista)
        */
        function concatErrorsMessage(res) {
            var i = 0, len, error, message = '',
            defaultMessage = 'Ocorreu um erro inesperado!',
            errors = [
                {
                    message: defaultMessage
                }
            ];

            if (res.data) {
                if (res.data.errors || res.data.erros) {
                    errors = res.data.errors || res.data.erros;
                } else if (res.data.reason) {
                    errors = [
                        {
                            message: res.data.reason
                        }
                    ];
                }
            }

            len = errors.length;

            if (len > 1) {
                for (i; i < len; i++) {
                    error = errors[i];
                    message += ('<p>' + error.message || error.mensagem + '</p>');
                }
            } else {
                message = '<p>' + (len === 1 && (errors[0].message || errors[0].mensagem) ? errors[0].message || errors[0].mensagem : defaultMessage)  + '</p>';
            }

            return message;
        }

        /**
        * @name rejectedRequest
        * @param  {Object} res Resposta recebida do servidor
        *
        * @description
        * Trata mensagens de erro recebidas do servidor
        */
        function rejectedRequest(res) {
            $ionicPopup.alert({
                title: 'Ops!',
                template: concatErrorsMessage(res)
            });

        }
    }
})();
