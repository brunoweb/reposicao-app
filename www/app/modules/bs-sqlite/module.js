(function() {
    'use strict';

    angular.module('bluesoft-sqlite', ['ngCordova', 'underscore', 'ngIOS9UIWebViewPatch']).service('bsSQlite', bsSQlite);

    bsSQlite.$inject = ['$cordovaSQLite', '_', '$q'];
    function bsSQlite($cordovaSQLite, _, $q) {
        window.SQLiteDb = window.SQLiteDb || (window.cordova ? $cordovaSQLite.openDB({name: 'bs-consulta-produtos.db', location: 'default'}) : window.openDatabase('bs-consulta-produtos.db', '1', 'bs-consulta-produtos.db', 1024 * 1024 * 100));
        var service, bsSQliteConfig = {
            schema: {
                name: {
                    type: 'text',
                    primary: true
                },
                keepTable: {
                    type: 'BOOLEAN NOT NULL DEFAULT 0'
                }
            },
            table: 'BS_SQLITE_TABLES'
        };

        service = {
            debug: false,
            select: select,
            selectUnique: selectUnique,
            create: create,
            createFromSchema: createFromSchema,
            remove: remove,
            insert: insert,
            insertCollection: insertCollection,
            insertObj: insertObj,
            insertObjCollection: insertObjCollection,
            update: update,
            clear: clear,
            clearAll: clearAll,
            drop: drop,
            dropAll: dropAll
        };

        return service;

        function getRows(response, unique) {
            var i = 0, rows = response.rows, length = rows.length,
            results = [];

            for (i; i < length; i++) {
                results.push(rows.item(i));
            }

            return unique ? results[0] : results;

        }

        function createTableConfig(config, foreign) {
            var key, configList = [];

            for (key in config) {
                if (config.hasOwnProperty(key)) {
                    configList.push(' ' + key + ' ' + config[key] + ' ');
                }
            }

            if (foreign) {
                configList.push(foreign);
            }

            return '(' + configList.join() + ')';
        }

        function createTableConfigFromSchema(config) {
            var key, configList = [], column, obj, primaryKeys = [];

            for (key in config) {
                if (config.hasOwnProperty(key)) {
                    obj = config[key];

                    column = [key, obj.type];

                    if (obj.primary) {
                        primaryKeys.push(key);
                    }

                    if (obj.foreign) {
                        column.push(obj.foreign);
                    }

                    configList.push(column.join(' '));
                }
            }

            if (primaryKeys.length > 0) {
                configList.push(' PRIMARY KEY ' + joinWithParentheses(primaryKeys));
            }

            return joinWithParentheses(configList);
        }

        function getObjKey(obj, label) {
            if (!label) {
                return null;
            }

            label = label.replace(/\[(\w+)\]/g, '.$1'); // converte index [0] para prop .0
            label = label.replace(/^\./, ''); // remove qualquer . no final da string

            var objCopy, labels, i = 0, _length, labelKey;

            objCopy = angular.copy(obj); // cria copia para evitar alterações no obj principal
            labels = label.split('.');
            _length = labels.length;

            // varre o obj até achar a ultima label válida da string, se não encontrar seta como null
            for (i; i < _length; i++) {
                labelKey = labels[i];
                if (labelKey in objCopy) {
                    objCopy = objCopy[labelKey];
                } else {
                    objCopy = null;
                    break;
                }
            }

            // devolve ultimo valor encontrado
            return objCopy;
        }

        function createInsertValuesFromObj(config) {
            var keys = [], result = {}, map = config.schema;

            keys = getKeysAsArray(map);

            result.keys = joinWithParentheses(keys);
            result.values = getValuesString(keys);
            result.params = getValuesFromMap(config.params, keys, map);

            return result;
        }

        function createInsertValuesFromObjCollection(config) {
            var param, keys = [], params = [], map = config.schema,
            result = {}, i = 0, len = config.params.length;

            keys = getKeysAsArray(map);

            for (i; i < len; i++) {
                param = config.params[i];
                params.push(getValuesFromMap(param, keys, map));
            }

            result.keys = joinWithParentheses(keys);
            result.values = getValuesString(keys);
            result.params = params;

            return result;
        }

        function getKeysAsArray(obj) {
            var keys = [], key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    keys.push(key);
                }
            }

            return keys;
        }

        function getValuesString(params) {
            var i = 0, values = [], len = params.length;

            for (i; i < len; i++) {
                values.push('?');
            }

            return ' values' + joinWithParentheses(values) + ' ';
        }

        function joinWithParentheses(list) {
            return '(' + list.join(', ') + ')';
        }

        function getValuesFromMap(obj, keys, map) {
            var i = 0, len = keys.length, values = [], valueKey, key, value;

            for (i; i < len; i++) {
                key = keys[i];
                valueKey = map[key].valueKey || key;
                value = getObjKey(obj, valueKey);
                if (map[key].type.toUpperCase().indexOf('BOOL') != -1) {
                    values.push(value ? 1 : 0);
                } else {
                    if (typeof value === 'undefined') {
                        values.push(null);
                    } else {
                        values.push(value);
                    }
                }
            }

            return values;
        }

        function executeQuery(config) {
            var defer = $q.defer(), params = [], sqlitePromise, cordovaFn, result;

            if (config.params) {
                params = params.concat(config.params);
            }

            cordovaFn = $cordovaSQLite[config.method] || $cordovaSQLite.execute;

            sqlitePromise = cordovaFn(window.SQLiteDb, config.queryStr, params);

            sqlitePromise.then(function(response) {
                result = getRows(response, config.unique);

                if (service.debug) {
                    console.groupCollapsed(config.queryStr);
                    console.info(config.queryStr);
                    console.info('Query params: ', params);
                    console.info('Result: ', result);
                    console.info('Config: ', config);
                    console.groupEnd();
                }

                defer.resolve(result);
            });

            sqlitePromise.catch(function(err) {
                console.group('ERRO ao executar query');
                console.warn(config.queryStr);
                console.warn('Code:', err.code, 'Message:', err.message);
                console.info('Config: ', config);
                console.error('Error: ', err);
                console.groupEnd();

                defer.reject(err);
            });

            return defer.promise;
        }

        function select(table, config, unique) {
            if (!config) {
                config = {};
            }
            var query = 'SELECT {select} FROM {table} ';

            query = query.replace('{select}', config.select || '*').replace('{table}', table.toUpperCase());
            query += (config.join || '') + ' ' + (config.condition || '');

            if (config.groupBy) {
                query += ' group by ' + (config.groupBy || '');
            }

            if (config.orderBy) {
                query += ' order by ' + (config.orderBy.columns.join(', ') || '') + ' ' + (config.orderBy.direction || 'ASC');
            }

            return executeQuery({
                queryStr: query,
                params: config.params,
                unique: unique
            });

        }

        function selectUnique(table, config) {
            return select(table, config, true);
        }

        function registerTable(table, config, keepTable) {
            var defer = $q.defer(),
            query = 'CREATE TABLE IF NOT EXISTS ' + bsSQliteConfig.table;

            query += createTableConfigFromSchema(bsSQliteConfig.schema);

            executeQuery({
                queryStr: query
            }).then(function() {

                insertObj(bsSQliteConfig.table, {
                    schema: bsSQliteConfig.schema,
                    params: {name: table, keepTable: keepTable}
                }).then(defer.resolve, defer.reject);

            }).catch(defer.reject);

            return defer.promise;
        }

        function create(table, tableConfig, foreign, keepTable) {
            registerTable(table, tableConfig, keepTable);

            var query = 'CREATE TABLE IF NOT EXISTS ' + table.toUpperCase();
            query += createTableConfig(tableConfig, foreign);

            return executeQuery({
                queryStr: query
            });
        }

        function createFromSchema(table, config, keepTable) {
            registerTable(table, config, keepTable);

            var query = 'CREATE TABLE IF NOT EXISTS ' + table.toUpperCase();
            query += createTableConfigFromSchema(config);

            return executeQuery({
                queryStr: query
            });
        }

        function remove(table, config) {
            var query = 'DELETE {select} FROM {table} ';
            query = query.replace('{select}', config.select || '').replace('{table}', table.toUpperCase());
            query += (config.condition || '');

            return executeQuery({
                queryStr: query,
                params: config.params
            });

        }

        function insert(table, config) {
            var query = 'INSERT OR REPLACE INTO {table} ';
            query = query.replace('{table}', table.toUpperCase());
            query += getValuesString(config.params);

            return executeQuery({
                queryStr: query,
                params: config.params
            });

        }

        function insertCollection(table, config) {
            var query = 'INSERT OR REPLACE INTO {table}';
            query = query.replace('{table}', table.toUpperCase());
            query += getValuesString(config.params[0]);

            return executeQuery({
                queryStr: query,
                params: config.params,
                method: 'insertCollection'
            });
        }

        function insertObj(table, config) {
            var query = 'INSERT OR REPLACE INTO {table} ', insertObject = createInsertValuesFromObj(config);

            query = query.replace('{table}', table.toUpperCase());
            query += insertObject.keys + ' ' + insertObject.values;

            return executeQuery({
                queryStr: query,
                params: insertObject.params
            });
        }

        function insertObjCollection(table, config) {
            var defer = $q.defer();
            if (!config.params || config.params.length === 0) {
                defer.resolve();
            } else {
                var query = 'INSERT OR REPLACE INTO {table} ', insertObject = createInsertValuesFromObjCollection(config);
                query = query.replace('{table}', table.toUpperCase());

                query += insertObject.keys + ' ' + insertObject.values;

                executeQuery({
                    queryStr: query,
                    params: insertObject.params,
                    method: 'insertCollection'
                }).then(defer.resolve, defer.reject);
            }

            return defer.promise;
        }

        function update(table, config) {
            var query = 'UPDATE {table} set ';
            query = query.replace('{table}', table.toUpperCase());
            query += config.set + ' ';
            query += (config.condition || '');

            return executeQuery({
                queryStr: query,
                params: config.params
            });
        }

        function clear(table) {
            var query = 'DELETE FROM {table}';

            query = query.replace('{table}', table.toUpperCase());

            return executeQuery({
                queryStr: query
            });
        }

        function clearAll() {
            var defer = $q.defer();

            select(bsSQliteConfig.table).then(function(res) {
                var tables = res;

                $q.all(_.map(tables, function(table) {
                    return clear(table.name);
                })).then(defer.resolve, defer.reject);

            }).catch(defer.reject);

            return defer.promise;
        }

        function drop(table) {
            var query = 'DROP TABLE IF EXISTS ' + table.toUpperCase();

            return executeQuery({
                queryStr: query
            });
        }

        function dropAll() {
            var defer = $q.defer();

            select(bsSQliteConfig.table, {
                condition: 'WHERE keepTable = 0'
            }).then(function(res) {
                var tables = res || [];

                tables.push({name: bsSQliteConfig.table});

                $q.all(_.map(tables, function(table) {
                    return drop(table.name);
                })).then(defer.resolve, defer.reject);

            }).catch(defer.reject);

            return defer.promise;
        }
    }
})();
