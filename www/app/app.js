(function() {
    'use strict';

    window.bluesoftApp = angular.module('bluesoft-app', [
        'ionic',
        'ngCordova',
        'underscore',
        'bluesoft-sqlite',
        'ngIOS9UIWebViewPatch',
        'ionicLazyLoad']
    );
})();
