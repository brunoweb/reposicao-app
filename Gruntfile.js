(function() {
    'use strict';

    module.exports = function(grunt) {
        var _files = {
            js: ['www/app/**/*.js'],
            scss: 'scss/**/*.scss'
        };

        grunt.loadNpmTasks('grunt-replace');
        require('load-grunt-tasks')(grunt);

        grunt.initConfig({
            pkg: require('./package.json'),
            notify: {
                show: {
                    message: 'Atenção'
                }
            },

            // js
            jscs: {
                src: _files.js,
                options: {
                    config: '.jscsrc'
                }
            },
            jshint: {
                options: {
                    unused: true,
                    forin: true,
                    newcap: true,
                    strict: true,
                    eqnull: true
                },
                all: _files.js
            },

            // css
            sass: {
                dev: {
                    options: {
                        style: 'expanded',
                        loadPath: [
                            'www/lib/ionic/scss/',
                            'www/app/commons/scss/'
                        ]
                    },
                    files: {
                        'www/css/ionic.app.css': 'scss/ionic.app.scss'
                    }
                }
            },

            // watch
            watch: {
                css: {
                    files: _files.scss,
                    tasks: ['sass']
                },
                scripts: {
                    files: _files.js,
                    tasks: ['jshint', 'jscs']
                }
            },

            karma: {
                unit: {
                    configFile: 'karma-config.js'
                }
            },
            ngtemplates: {
                app: {
                    cwd: 'www/',
                    src: 'app/**/*.html',
                    dest: 'templates.js',
                    options: {
                        module: 'bluesoft-app',
                        prefix: ''
                    }
                }
            },

            //replace APIEndPoint
            replace: {
                dist: {
                    options: {
                        patterns: [
                            {
                                match: 'http://localhost:8100/erp-web/',
                                replacement: 'https://erp-2a.bluesoft.com.br/{0}'
                            }
                        ]
                    },
                    files: [
                        {expand: true, flatten: true, src: ['www/app/core/constant/api-endpoint.js'], dest: 'build/'}
                    ]
                }
            }
        });

        grunt.registerTask('test', [
            'jsvalidate',
            'ngtemplates',
            'karma'
        ]);

        grunt.registerTask('dev-replace-apiendpoint', 'replace');
        grunt.registerTask('jsvalidate', ['jshint', 'jscs']);
        grunt.registerTask('dev', ['jsvalidate', 'sass', 'watch']);
    };
})();
